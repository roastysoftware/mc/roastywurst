package pogfactory.roasty.mc.client.math.utilities;

import net.minecraft.client.util.math.Vector3f;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.Quaternion;

public final class RotationUtils {

    public static Quaternion rotation(float yaw, float pitch) {
        Quaternion rotation = Quaternion.IDENTITY.copy();
        rotation.hamiltonProduct(Vector3f.POSITIVE_Y.getDegreesQuaternion(-yaw));
        rotation.hamiltonProduct(Vector3f.POSITIVE_X.getDegreesQuaternion(-pitch));
        return rotation;
    }

    public static Quaternion rotation(Entity entity) {
        return rotation(entity.yaw, entity.pitch);
    }

}
