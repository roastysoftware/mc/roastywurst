package pogfactory.roasty.mc.client.utility.color;

public final class ColorUtils {

    public static ColorComponents extractColorComponents(int color) {
        return new ColorComponents(
                (float) (color >> 16 & 255) / 255.0F,
                (float) (color >> 8 & 255) / 255.0F,
                (float) (color & 255) / 255.0F,
                (float) (color >> 24 & 255) / 255.0F
        );
    }

}
