package pogfactory.roasty.mc.client.utility.color;

public final class ColorComponents {

    public float red;
    public float green;
    public float blue;
    public float alpha;

    public ColorComponents(float red, float green, float blue) {
        this(red, green, blue, 1f);
    }

    public ColorComponents(float red, float green, float blue, float alpha) {
        if (red <= 1f) {
            this.red = red;
        } else if (red <= 255f) {
            this.red = red / 255f;
        }
        if (green <= 1f) {
            this.green = green;
        } else if (green <= 255f) {
            this.green = green / 255f;
        }
        if (blue <= 1f) {
            this.blue = blue;
        } else if (blue <= 255f) {
            this.blue = blue / 255f;
        }
        if (alpha <= 1f) {
            this.alpha = alpha;
        } else if (alpha <= 255f) {
            this.alpha = alpha / 255f;
        }
    }

}
