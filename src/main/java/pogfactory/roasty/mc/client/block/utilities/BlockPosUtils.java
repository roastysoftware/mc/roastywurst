package pogfactory.roasty.mc.client.block.utilities;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public final class BlockPosUtils {

    public static Vec3d toVec3d(BlockPos pos) {
        return new Vec3d(pos.getX(), pos.getY(), pos.getZ());
    }

    public static BlockPos fromVec3d(Vec3d pos) {
        return BlockPos.ORIGIN.add(pos.x, pos.y, pos.z);
    }

}
