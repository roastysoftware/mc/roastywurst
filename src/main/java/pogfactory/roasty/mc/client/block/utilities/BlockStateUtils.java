package pogfactory.roasty.mc.client.block.utilities;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.Direction;

public final class BlockStateUtils {

    public static Direction getDirection(BlockState blockState) {
        return (Direction) blockState.getEntries().keySet().stream()
                .map(blockState::get)
                .filter(propValue -> propValue instanceof Direction)
                .findFirst()
                .orElse(null);
    }

}
