package pogfactory.roasty.mc.client.render;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.Camera;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.*;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.wurstclient.events.WorldRenderListener;
import net.wurstclient.util.RotationUtils;
import pogfactory.roasty.mc.client.block.utilities.BlockPosUtils;
import pogfactory.roasty.mc.client.render.utilities.CameraUtils;
import pogfactory.roasty.mc.client.render.utilities.RenderLayers;
import pogfactory.roasty.mc.client.render.utilities.VoxelFactory;
import pogfactory.roasty.mc.client.utility.color.ColorComponents;

public class TargetLineRenderBuilder {

    private static final MinecraftClient MC = MinecraftClient.getInstance();

    Camera camera;
    Vec3d pos;
    Vec3d cameraPos;

    MatrixStack matrixStack;
    VertexConsumerProvider.Immediate immediate;

    ColorComponents color;
    double width = 1;

    public TargetLineRenderBuilder(MatrixStack matrixStack, VertexConsumerProvider.Immediate immediate, Camera camera) {
        this.matrixStack = matrixStack;
        this.immediate = immediate;
        this.camera = camera;
        this.cameraPos = camera.getPos();
    }

    public TargetLineRenderBuilder(WorldRenderListener.WorldRenderEvent worldRenderEvent) {
        this(worldRenderEvent.matrixStack, worldRenderEvent.immediate, worldRenderEvent.camera);
    }

    public static TargetLineRenderBuilder build(WorldRenderListener.WorldRenderEvent worldRenderEvent) {
        return new TargetLineRenderBuilder(worldRenderEvent);
    }

    public TargetLineRenderBuilder target(Vec3d pos) {
        this.pos = pos;
        return this;
    }

    public TargetLineRenderBuilder target(BlockPos pos) {
        return target(BlockPosUtils.toVec3d(pos));
    }

    public TargetLineRenderBuilder target(Vec3i pos) {
        return target(new Vec3d(pos.getX(), pos.getY(), pos.getZ()));
    }

    public TargetLineRenderBuilder target(Entity entity) {
        return target(entity.getPos().add(0, entity.getHeight() / 2, 0));
    }

    public TargetLineRenderBuilder color(ColorComponents color) {
        this.color = color;
        return this;
    }

    public TargetLineRenderBuilder strokeWidth(double width) {
        this.width = width;
        return this;
    }

    public void draw() {
        prepareDraw();
        applyTransformations();
        drawLine();
        finalizeDraw();
    }

    void prepareDraw() {
        matrixStack.push();
    }

    void applyTransformations() {

    }

    private void drawLine() {
        VertexConsumer vertexConsumer = immediate.getBuffer(RenderLayers.lines(width));
        Matrix4f model = matrixStack.peek().getModel();
        Vec3d cameraCenter = CameraUtils.getFacingCameraCenter(camera);

        vertexConsumer.vertex(model,
                (float)(cameraCenter.x - cameraPos.x),
                (float)(cameraCenter.y - cameraPos.y),
                (float)(cameraCenter.z - cameraPos.z))
                .color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model,
                (float)(pos.x - cameraPos.x), (float)(pos.y - cameraPos.y), (float)(pos.z - cameraPos.z))
                .color(color.red, color.green, color.blue, color.alpha).next();
    }

    void finalizeDraw() {
        matrixStack.pop();
    }

}
