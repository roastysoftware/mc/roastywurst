package pogfactory.roasty.mc.client.render.utilities;

import net.minecraft.client.render.Camera;
import net.minecraft.util.math.Vec3d;

public final class CameraUtils {

    public static Vec3d getFacingCameraCenter(Camera camera) {
        Vec3d cameraPos = camera.getPos();
        Vec3d crosshair = new Vec3d(cameraPos.x, cameraPos.y, cameraPos.z);
        Vec3d distanceToCrosshair = new Vec3d(0, 0, 5.0)
                .rotateX((float)Math.toRadians(camera.getPitch()))
                .rotateY((float)Math.toRadians(180 - camera.getYaw()));
        return crosshair.subtract(distanceToCrosshair);
    }

}
