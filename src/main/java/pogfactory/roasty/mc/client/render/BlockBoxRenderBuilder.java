package pogfactory.roasty.mc.client.render;

import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.Camera;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShape;
import net.wurstclient.events.WorldRenderListener;
import net.wurstclient.util.BlockUtils;
import pogfactory.roasty.mc.client.block.utilities.BlockPosUtils;
import pogfactory.roasty.mc.client.render.utilities.VoxelFactory;

public class BlockBoxRenderBuilder extends BoxRenderBuilder {

    private static final MinecraftClient MC = MinecraftClient.getInstance();

    private boolean dontRender;
    private final Entity entity;
    private BoxRenderer.RenderOptions renderOptions;
    private BlockState blockState;

    public BlockBoxRenderBuilder(
            MatrixStack matrixStack, VertexConsumerProvider.Immediate immediate, Camera camera,
            Entity entity) {
        super(matrixStack, immediate, camera);
        this.entity = entity;
    }

    public BlockBoxRenderBuilder(WorldRenderListener.WorldRenderEvent worldRenderEvent) {
        this(worldRenderEvent.matrixStack, worldRenderEvent.immediate, worldRenderEvent.camera, MC.player);
    }

    public static BlockBoxRenderBuilder build(WorldRenderListener.WorldRenderEvent event) {
        return new BlockBoxRenderBuilder(event);
    }

    public BlockBoxRenderBuilder options(BoxRenderer.RenderOptions options) {
        this.renderOptions = options;
        return this;
    }

    public BlockBoxRenderBuilder block(BlockPos blockPos) {
        blockState = BlockUtils.getState(blockPos);

        BoxRenderer.ChestOptions chestOptions = BoxRenderer.ChestOptions.fromBlockState(blockState);
        if (chestOptions.skipRight) {
            this.dontRender = true;
            return this;
        }

        if (renderOptions == null) {
            renderOptions = new BoxRenderer.RenderOptions();
        }

        position(blockPos);

        VoxelShape shape = VoxelFactory.makeVoxelShape(blockState, blockPos, null, entity, renderOptions);

        return (BlockBoxRenderBuilder) shape(VoxelFactory.chestVoxelShape(shape, blockState));
    }

    @Override
    public void draw() {
        if (!dontRender) {
            super.draw();
        }
    }
}
