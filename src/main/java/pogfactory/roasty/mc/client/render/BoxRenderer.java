package pogfactory.roasty.mc.client.render;

import net.minecraft.block.BlockState;
import net.minecraft.block.enums.ChestType;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.*;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.*;
import net.minecraft.util.shape.VoxelShape;
import net.wurstclient.events.WorldRenderListener;
import net.wurstclient.util.BlockUtils;
import pogfactory.roasty.mc.client.block.utilities.BlockPosUtils;
import pogfactory.roasty.mc.client.render.utilities.RenderLayers;
import pogfactory.roasty.mc.client.render.utilities.VoxelFactory;
import pogfactory.roasty.mc.client.utility.color.ColorComponents;

import javax.annotation.Nullable;
import java.util.Optional;

public final class BoxRenderer {

    private static final MinecraftClient MC = MinecraftClient.getInstance();

    public static void drawBlockOutline(
            BlockPos blockPos, VertexConsumerProvider.Immediate immediate, MatrixStack matrixStack, Camera camera,
            RenderOptions renderOptions, ColorComponents color) {
        BlockState blockState = BlockUtils.getState(blockPos);
        VertexConsumer vertexConsumer = immediate.getBuffer(pogfactory.roasty.mc.client.render.utilities.RenderLayers.lines());
        Vec3d cameraPos = camera.getPos();
        drawBlockOutline(blockPos, blockState, camera.getFocusedEntity(), renderOptions, matrixStack, vertexConsumer,
                cameraPos.x, cameraPos.y, cameraPos.z, color);
    }

    public static void drawBlockOutline(
            BlockPos blockPos, BlockState blockState, Entity entity, RenderOptions renderOptions,
            MatrixStack matrixStack, VertexConsumer vertexConsumer, double x, double y, double z,
            ColorComponents color) {
        ChestOptions chestOptions = ChestOptions.fromBlockState(blockState);
        if (chestOptions.skipRight) {
            return;
        }

        VoxelShape shape = VoxelFactory.makeVoxelShape(blockState, blockPos, null, entity, renderOptions);

        drawBoxOutline(BlockPosUtils.toVec3d(blockPos), new Vec3d(x, y, z),
                matrixStack, vertexConsumer, VoxelFactory.chestVoxelShape(shape, blockState), color);
    }

    public static void drawBoxOutline(
            Vec3d pos, Camera camera, MatrixStack matrixStack, VertexConsumer vertexConsumer,
            VoxelShape shape, ColorComponents color) {
        drawBoxOutline(pos, camera.getPos(), matrixStack, vertexConsumer, shape, color);
    }

    public static void drawBoxOutline(
            Vec3d pos, Camera camera, Quaternion rotation, MatrixStack matrixStack, VertexConsumer vertexConsumer,
            VoxelShape shape, ColorComponents color) {
        drawBoxOutline(pos, camera.getPos(), rotation, matrixStack, vertexConsumer, shape, color);
    }


    public static void drawBoxOutline(
            Vec3d pos, Vec3d camPos, MatrixStack matrixStack, VertexConsumer vertexConsumer,
            VoxelShape shape, ColorComponents color) {
        drawBoxOutlineDirect(
                matrixStack, vertexConsumer, shape,
                pos.x - camPos.x, pos.y - camPos.y, pos.z - camPos.z, color);
    }

    public static void drawBoxOutline(
            Vec3d pos, Vec3d camPos, Quaternion rotation, MatrixStack matrixStack, VertexConsumer vertexConsumer,
            VoxelShape shape, ColorComponents color) {
        drawBoxOutlineDirect(
                matrixStack, vertexConsumer, shape,
                pos.x - camPos.x, pos.y - camPos.y, pos.z - camPos.z, color, rotation);
    }

    private static void drawBoxOutlineDirect(
            MatrixStack matrixStack, VertexConsumer vertexConsumer, VoxelShape shape,
            double x, double y, double z, ColorComponents color) {
        drawBoxOutlineDirect(matrixStack, vertexConsumer, shape, x, y, z, color, null);
    }

    private static void drawBoxOutlineDirect(
            MatrixStack matrixStack, VertexConsumer vertexConsumer,
            VoxelShape voxelShape, double x, double y, double z, ColorComponents color,
            @Nullable Quaternion rotation) {
        matrixStack.push();
        if (rotation != null) {
            matrixStack.multiply(rotation);
        }
        matrixStack.translate(x, y, z);
        drawBoxOutlineDirect(matrixStack, vertexConsumer, voxelShape, color);
        matrixStack.pop();
    }

    protected static void drawBoxOutlineDirect(
            MatrixStack matrixStack, VertexConsumer vertexConsumer, VoxelShape voxelShape, ColorComponents color) {
        Matrix4f model = matrixStack.peek().getModel();
        voxelShape.forEachEdge((exs, eys, ezs, exe, eye, eze) -> {
            vertexConsumer.vertex(model, (float)exs, (float)eys, (float)ezs)
                    .color(color.red, color.green, color.blue, color.alpha).next();
            vertexConsumer.vertex(model, (float)exe, (float)eye, (float)eze)
                    .color(color.red, color.green, color.blue, color.alpha).next();
        });
    }

    public static void drawFilledBlockShape(
            BlockPos blockPos, VertexConsumerProvider.Immediate immediate, MatrixStack matrixStack, Camera camera,
            RenderOptions renderOptions, ColorComponents color) {
        matrixStack.push();
        BlockState blockState = BlockUtils.getState(blockPos);
        Vec3d cameraPos = camera.getPos();
        drawFilledBlockShape(blockPos, blockState, camera.getFocusedEntity(), renderOptions, matrixStack,
                immediate.getBuffer(pogfactory.roasty.mc.client.render.utilities.RenderLayers.fill()),
                cameraPos.x, cameraPos.y, cameraPos.z, color);
        matrixStack.pop();
    }


    public static void drawFilledShape(MatrixStack matrixStack,
                                       VertexConsumer vertexConsumer, VoxelShape voxelShape, ColorComponents color) {
        Box box = voxelShape.getBoundingBox();
        Matrix4f model = matrixStack.peek().getModel();
        float minX = (float) box.minX;
        float minY = (float) box.minY;
        float minZ = (float) box.minZ;
        float maxX = (float) box.maxX;
        float maxY = (float) box.maxY;
        float maxZ = (float) box.maxZ;

        // cube left
        vertexConsumer.vertex(model, minX, minY, minZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, maxX, minY, minZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, maxX, maxY, minZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, minX, maxY, minZ).color(color.red, color.green, color.blue, color.alpha).next();

        // cube front
        vertexConsumer.vertex(model, minX, minY, minZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, minX, minY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, minX, maxY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, minX, maxY, minZ).color(color.red, color.green, color.blue, color.alpha).next();

        // cube right
        vertexConsumer.vertex(model, maxX, minY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, minX, minY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, minX, maxY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, maxX, maxY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();

        // cube back
        vertexConsumer.vertex(model, maxX, minY, minZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, maxX, minY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, maxX, maxY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, maxX, maxY, minZ).color(color.red, color.green, color.blue, color.alpha).next();

        // cube top
        vertexConsumer.vertex(model, minX, maxY, minZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, maxX, maxY, minZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, maxX, maxY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, minX, maxY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();

        // cube bottom
        vertexConsumer.vertex(model, minX, minY, minZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, maxX, minY, minZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, maxX, minY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();
        vertexConsumer.vertex(model, minX, minY, maxZ).color(color.red, color.green, color.blue, color.alpha).next();
    }

    public static void drawFilledBlockShape(
            BlockPos blockPos, BlockState blockState, Entity entity, RenderOptions renderOptions,
            MatrixStack matrixStack, VertexConsumer vertexConsumer, double x, double y, double z,
            ColorComponents color) {

        ChestOptions chestOptions = ChestOptions.fromBlockState(blockState);
        if (chestOptions.skipRight) {
            return;
        }

        VoxelShape shape = VoxelFactory.makeVoxelShape(blockState, blockPos, null, entity, renderOptions);

        shape = VoxelFactory.chestVoxelShape(shape, blockState);

        drawFilledBoxShape(BlockPosUtils.toVec3d(blockPos), new Vec3d(x, y, z), matrixStack, vertexConsumer, shape, color);
    }

    public static void drawFilledBoxShape(
            Vec3d pos, Camera camera, MatrixStack matrixStack, VertexConsumer vertexConsumer,
            VoxelShape shape, ColorComponents color) {
        drawFilledBoxShape(pos, camera.getPos(), matrixStack, vertexConsumer, shape, color);
    }

    public static void drawFilledBoxShape(
            Vec3d pos, Vec3d camPos, MatrixStack matrixStack,
            VertexConsumer vertexConsumer, VoxelShape shape, ColorComponents color) {
        matrixStack.push();
        matrixStack.translate(pos.x - camPos.x, pos.y - camPos.y, pos.z - camPos.z);
        drawFilledShape(matrixStack, vertexConsumer, shape, color);
        matrixStack.pop();
    }

    public static void drawFullBlockBox(BlockPos blockPos, VertexConsumerProvider.Immediate immediate,
                                        MatrixStack matrixStack, Camera camera, RenderOptions renderOptions,
                                        ColorComponents outlineColor, ColorComponents fillColor) {
        drawBlockOutline(blockPos, immediate, matrixStack, camera, renderOptions, outlineColor);
        drawFilledBlockShape(blockPos, immediate, matrixStack, camera, renderOptions, fillColor);
    }

    public static void drawFullBlockBox(BlockPos blockPos, VertexConsumerProvider.Immediate immediate,
                                        MatrixStack matrixStack, Camera camera,
                                        ColorComponents outlineColor, ColorComponents fillColor) {
        drawFullBlockBox(blockPos, immediate, matrixStack, camera, new RenderOptions(), outlineColor, fillColor);
    }

    public static void drawFullBox(Vec3d pos, VertexConsumerProvider.Immediate immediate,
                                   MatrixStack matrixStack, Camera camera, VoxelShape shape,
                                   ColorComponents outlineColor, ColorComponents fillColor) {
        drawBoxOutline(pos, camera, matrixStack, immediate, shape, outlineColor);
        drawFilledBoxShape(pos, camera, matrixStack, immediate, shape, fillColor);
    }

    public static void drawBoxOutline(
            Vec3d pos, Camera camera, MatrixStack matrixStack,
            VertexConsumerProvider.Immediate immediate, VoxelShape shape, ColorComponents color) {
        drawBoxOutline(pos, camera, matrixStack, immediate.getBuffer(pogfactory.roasty.mc.client.render.utilities.RenderLayers.lines()), shape, color);
    }

    public static void drawFilledBoxShape(
            Vec3d pos, Camera camera, MatrixStack matrixStack,
            VertexConsumerProvider.Immediate immediate, VoxelShape shape, ColorComponents color) {
        drawFilledBoxShape(pos, camera, matrixStack, immediate.getBuffer(pogfactory.roasty.mc.client.render.utilities.RenderLayers.fill()),
                shape, color);
    }

    public static void drawBoxOutline(
            Vec3d pos, Box box, Camera camera, MatrixStack matrixStack,
            VertexConsumerProvider.Immediate immediate,  ColorComponents color) {
        drawBoxOutline(pos, camera, matrixStack, immediate, VoxelFactory.fromBox(box), color);
    }

    public static void drawBoxOutline(
            Vec3d pos, Box box, Vec3d rotation, Camera camera, MatrixStack matrixStack,
            VertexConsumerProvider.Immediate immediate,  ColorComponents color) {
        drawBoxOutline(pos, camera, new Quaternion((float)rotation.x, (float)rotation.y * 360f, (float)rotation.z, true),
                matrixStack, immediate, VoxelFactory.fromBox(box), color);
    }

    private static void drawBoxOutline(
            Vec3d pos, Camera camera, Quaternion rotation, MatrixStack matrixStack,
            VertexConsumerProvider.Immediate immediate, VoxelShape shape, ColorComponents color) {
        drawBoxOutline(pos, camera, rotation, matrixStack, immediate.getBuffer(RenderLayers.lines()), shape, color);
    }

    public static final class ChestOptions {
        public boolean enlargeChest;
        public boolean skipRight;

        public static ChestOptions fromBlockState(BlockState blockState) {
            ChestOptions chestOptions = new ChestOptions();
            Optional<?> chestTypeOptional = blockState.getEntries().keySet().stream()
                    .map(blockState::get)
                    .filter(propValue -> propValue instanceof ChestType)
                    .findFirst();

            if (chestTypeOptional.isPresent() && chestTypeOptional.get() instanceof ChestType) {
                ChestType chestType = (ChestType) chestTypeOptional.get();
                switch (chestType) {
                    case LEFT:
                        chestOptions.enlargeChest = true;
                        break;
                    case RIGHT:
                        chestOptions.skipRight = true;
                        break;
                    default:
                    case SINGLE:
                }
            }

            return chestOptions;
        }
    }

    public static final class RenderOptions {
        private Double overwriteWidth;
        private Double overwriteHeight;
        private VoxelShape overwriteVoxelShape;

        public RenderOptions() {

        }

        public RenderOptions(@Nullable Double overwriteWidth, @Nullable Double overwriteHeight) {
            this.overwriteWidth = overwriteWidth;
            this.overwriteHeight = overwriteHeight;
        }

        public RenderOptions(VoxelShape shape) {
            overwriteVoxelShape = shape;
        }

        public Double getOverwriteHeight() {
            return overwriteHeight;
        }

        public Double getOverwriteWidth() {
            return overwriteWidth;
        }

        public VoxelShape getOverwriteVoxelShape() {
            return overwriteVoxelShape;
        }
    }

}
