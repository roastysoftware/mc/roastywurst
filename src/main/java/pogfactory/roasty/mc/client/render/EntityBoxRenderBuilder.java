package pogfactory.roasty.mc.client.render;

import net.minecraft.client.render.Camera;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.Box;
import net.minecraft.util.shape.VoxelShapes;
import net.wurstclient.events.WorldRenderListener;

public class EntityBoxRenderBuilder extends BoxRenderBuilder {

    public EntityBoxRenderBuilder(MatrixStack matrixStack, VertexConsumerProvider.Immediate immediate, Camera camera) {
        super(matrixStack, immediate, camera);
    }

    public EntityBoxRenderBuilder(WorldRenderListener.WorldRenderEvent worldRenderEvent) {
        super(worldRenderEvent);
    }

    public static EntityBoxRenderBuilder build(WorldRenderListener.WorldRenderEvent event) {
        return new EntityBoxRenderBuilder(event);
    }

    public EntityBoxRenderBuilder entity(Entity entity) {
        position(entity.getPos());
        Box box = entity.getBoundingBox();
        if (box != null) {
            box = new Box(
                    box.minX - pos.x, box.minY - pos.y, box.minZ - pos.z,
                    box.maxX - pos.x, box.maxY - pos.y, box.maxZ - pos.z
            );
        }
        if (box == null) {
            box = new Box(0, 0, 0, entity.getWidth(), entity.getHeight(), entity.getWidth());
        }
        shape(box);
        rotate(entity, true, false);
        return this;
    }

}
