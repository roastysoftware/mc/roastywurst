package pogfactory.roasty.mc.client.render.utilities;

import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import pogfactory.roasty.mc.client.block.utilities.BlockStateUtils;
import pogfactory.roasty.mc.client.render.BoxRenderer;

import javax.annotation.Nullable;

public final class VoxelFactory {

    private static final MinecraftClient MC = MinecraftClient.getInstance();

    public static VoxelShape makeVoxelShape(
            BlockState blockState, @Nullable BlockPos blockPos, @Nullable Vec3d pos,
            Entity entity, BoxRenderer.RenderOptions renderOptions) {
        if (renderOptions.getOverwriteVoxelShape() != null) {
            return renderOptions.getOverwriteVoxelShape();
        }

        VoxelShape voxelShape = null;

        if (blockPos != null) {
            voxelShape = blockState.getOutlineShape(MC.world, blockPos, ShapeContext.of(entity));
        }
        if (voxelShape == null || voxelShape.isEmpty()) {
            if (renderOptions.getOverwriteVoxelShape() != null) {
                voxelShape = renderOptions.getOverwriteVoxelShape();
            } else {
                voxelShape = VoxelShapes.fullCube();
            }
        }

        if (renderOptions.getOverwriteHeight() != null) {
            voxelShape = VoxelShapes.cuboid(
                    voxelShape.getMin(Direction.Axis.X),
                    voxelShape.getMin(Direction.Axis.Y),
                    voxelShape.getMin(Direction.Axis.Z),
                    voxelShape.getMax(Direction.Axis.X),
                    renderOptions.getOverwriteHeight(),
                    voxelShape.getMax(Direction.Axis.Z)
            );
        }
        if (renderOptions.getOverwriteWidth() != null) {
            voxelShape = VoxelShapes.cuboid(
                    voxelShape.getMin(Direction.Axis.X),
                    voxelShape.getMin(Direction.Axis.Y),
                    voxelShape.getMin(Direction.Axis.Z),
                    voxelShape.getMax(Direction.Axis.X),
                    voxelShape.getMax(Direction.Axis.Y),
                    renderOptions.getOverwriteWidth()
            );
        }
        return voxelShape;
    }

    public static VoxelShape addToVoxelShape(VoxelShape voxelShape, Vec3d extension, boolean centered) {
        if (centered) {
            return VoxelShapes.cuboid(
                    voxelShape.getMin(Direction.Axis.X) - extension.x / 2d,
                    voxelShape.getMin(Direction.Axis.Y) - extension.x / 2d,
                    voxelShape.getMin(Direction.Axis.Z) - extension.x / 2d,
                    voxelShape.getMax(Direction.Axis.X) + extension.x / 2d,
                    voxelShape.getMax(Direction.Axis.Y) + extension.y / 2d,
                    voxelShape.getMax(Direction.Axis.Z) + extension.z / 2d
            );
        } else {
            return VoxelShapes.cuboid(
                    voxelShape.getMin(Direction.Axis.X),
                    voxelShape.getMin(Direction.Axis.Y),
                    voxelShape.getMin(Direction.Axis.Z),
                    voxelShape.getMax(Direction.Axis.X) + extension.x,
                    voxelShape.getMax(Direction.Axis.Y) + extension.y,
                    voxelShape.getMax(Direction.Axis.Z) + extension.z
            );
        }
    }

    public static VoxelShape addToVoxelShape(VoxelShape voxelShape, Vec3d extension) {
        return addToVoxelShape(voxelShape, extension, false);
    }

    public static VoxelShape scaleVoxelShape(VoxelShape voxelShape, Vec3d scales, boolean center) {
        Box box = voxelShape.getBoundingBox();
        if (center) {
            return VoxelShapes.cuboid(
                    box.minX + ((box.getXLength()) * scales.x / 2d),
                    box.minY + ((box.getYLength()) * scales.x / 2d),
                    box.minZ + ((box.getZLength()) * scales.x / 2d),
                    (box.minX + box.getXLength()) * scales.x / 2d,
                    (box.minY + box.getYLength()) * scales.y / 2d,
                    (box.minZ + box.getZLength()) * scales.z / 2d
            );
        } else {
            return VoxelShapes.cuboid(
                    scales.x <= 0.0f ? box.maxX + box.getXLength() * scales.x : box.minX,
                    scales.y <= 0.0f ? box.maxY + box.getYLength() * scales.y : box.minY,
                    scales.z <= 0.0f ? box.maxZ + box.getZLength() * scales.z : box.minZ,
                    scales.x >= 0.0f ? box.minX + box.getXLength() * scales.x : box.maxX,
                    scales.y >= 0.0f ? box.minY + box.getYLength() * scales.y : box.maxY,
                    scales.z >= 0.0f ? box.minZ + box.getZLength() * scales.z : box.maxZ
            );
        }
    }


    public static VoxelShape scaleVoxelShape(VoxelShape voxelShape, Vec3d scales) {
        return scaleVoxelShape(voxelShape, scales, false);
    }

    public static VoxelShape scaleVoxelShape(VoxelShape voxelShape, double scale) {
        return scaleVoxelShape(voxelShape, scale, false);
    }

    public static VoxelShape scaleVoxelShape(VoxelShape voxelShape, double scale, boolean center) {
        return scaleVoxelShape(voxelShape, new Vec3d(scale, scale, scale), center);
    }

    public static VoxelShape chestVoxelShape(VoxelShape shape, BlockState blockState) {
        BoxRenderer.ChestOptions chestOptions = BoxRenderer.ChestOptions.fromBlockState(blockState);
        if (!chestOptions.enlargeChest) {
            return shape;
        }

        double xScale = 1;
        double zScale = 1;

        Direction direction = BlockStateUtils.getDirection(blockState);
        switch (direction) {
            case NORTH:
                xScale = 2;
                break;
            case EAST:
                zScale = 2;
                break;
            case SOUTH:
                xScale = -2;
                break;
            case WEST:
                zScale = -2;
                break;
            default:
                break;
        }

        return VoxelFactory.scaleVoxelShape(shape, new Vec3d(xScale, 1, zScale));
    }

    public static VoxelShape fromBox(Box box) {
        return VoxelShapes.cuboid(box);
    }

    public static VoxelShape center(VoxelShape shape, boolean horizontally, boolean vertically) {
        return center(shape, horizontally, vertically, horizontally);
    }

    public static VoxelShape center(VoxelShape shape, boolean centerX, boolean centerY, boolean centerZ) {
        Box bb = shape.getBoundingBox();
        double xLen = bb.getXLength();
        double yLen = bb.getYLength();
        double zLen = bb.getZLength();
        VoxelShape nextFullBlockShape = scaleVoxelShape(VoxelShapes.fullCube(),
                new Vec3d(Math.ceil(xLen), Math.ceil(yLen), Math.ceil(zLen)));
        Box nextFullBlock = nextFullBlockShape.getBoundingBox();
        double fullXLen = nextFullBlock.getXLength();
        double fullYLen = nextFullBlock.getYLength();
        double fullZLen = nextFullBlock.getZLength();
        double sideOffsetX = (fullXLen - xLen) / 2d;
        double sideOffsetY = (fullYLen - yLen) / 2d;
        double sideOffsetZ = (fullZLen - zLen) / 2d;
        return VoxelShapes.cuboid(
                centerX ? sideOffsetX : shape.getMin(Direction.Axis.X),
                centerY ? sideOffsetY : shape.getMin(Direction.Axis.Y),
                centerZ ? sideOffsetZ : shape.getMin(Direction.Axis.Z),
                centerX ? fullXLen - sideOffsetX : shape.getMax(Direction.Axis.X),
                centerY ? fullYLen - sideOffsetY : shape.getMax(Direction.Axis.Y),
                centerZ ? fullZLen - sideOffsetZ : shape.getMax(Direction.Axis.Z)
        );
    }

    public static VoxelShape center(VoxelShape shape) {
        return center(shape, true, true, true);
    }

}
