package pogfactory.roasty.mc.client.render;

import net.minecraft.client.render.Camera;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.*;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.wurstclient.events.WorldRenderListener;
import pogfactory.roasty.mc.client.block.utilities.BlockPosUtils;
import pogfactory.roasty.mc.client.math.utilities.RotationUtils;
import pogfactory.roasty.mc.client.render.utilities.RenderLayers;
import pogfactory.roasty.mc.client.render.utilities.VoxelFactory;
import pogfactory.roasty.mc.client.utility.color.ColorComponents;

public class BoxRenderBuilder {

    Camera camera;
    Vec3d pos;
    Vec3d cameraPos;

    MatrixStack matrixStack;
    VertexConsumerProvider.Immediate immediate;

    boolean centerQuad;
    int centerQuadX;
    int centerQuadZ;
    boolean centerShapeX;
    boolean centerShapeY;
    boolean centerShapeZ;
    boolean outline;
    boolean fill;
    Quaternion rotation;
    ColorComponents fillColor;
    ColorComponents outlineColor;

    VoxelShape shape = VoxelShapes.fullCube();

    public BoxRenderBuilder(MatrixStack matrixStack, VertexConsumerProvider.Immediate immediate, Camera camera) {
        this.matrixStack = matrixStack;
        this.immediate = immediate;
        this.camera = camera;
        this.cameraPos = camera.getPos();
    }

    public BoxRenderBuilder(WorldRenderListener.WorldRenderEvent worldRenderEvent) {
        this(worldRenderEvent.matrixStack, worldRenderEvent.immediate, worldRenderEvent.camera);
    }

    public BoxRenderBuilder position(Vec3d pos) {
        this.pos = pos;
        return this;
    }

    public BoxRenderBuilder position(BlockPos pos) {
        return position(BlockPosUtils.toVec3d(pos));
    }

    public BoxRenderBuilder position(Vec3i pos) {
        return position(new Vec3d(pos.getX(), pos.getY(), pos.getZ()));
    }

    public BoxRenderBuilder shape(Vec3d shapeVector) {
        return shape(VoxelShapes.cuboid(0, 0, 0, shapeVector.x, shapeVector.y, shapeVector.z));
    }

    public BoxRenderBuilder shape(VoxelShape shape) {
        this.shape = shape;
        return this;
    }

    public BoxRenderBuilder shape(Box box) {
        return shape(VoxelFactory.fromBox(box));
    }

    public BoxRenderBuilder shape(double startOffset, double uniformSize) {
        return shape(VoxelShapes.cuboid(startOffset, startOffset, startOffset,
                startOffset + uniformSize, startOffset + uniformSize, startOffset + uniformSize));
    }

    public BoxRenderBuilder shape(double uniformSize) {
        return shape(0, uniformSize);
    }

    public BoxRenderBuilder scaleShape(double scale) {
        return shape(VoxelFactory.scaleVoxelShape(shape, scale));
    }

    public BoxRenderBuilder scaleShape(Vec3d scales) {
        return shape(VoxelFactory.scaleVoxelShape(shape, scales));
    }

    public BoxRenderBuilder centerShape(boolean x, boolean y, boolean z) {
        this.centerShapeX = x;
        this.centerShapeY = y;
        this.centerShapeZ = z;
        return this;
    }

    public BoxRenderBuilder centerShape(boolean horizontally, boolean vertically) {
        this.centerShapeX = this.centerShapeZ = horizontally;
        this.centerShapeY = vertically;
        return this;
    }

    public BoxRenderBuilder centerShape() {
        return centerShape(true, true, true);
    }

    public BoxRenderBuilder centerQuad(int xDirection, int zDirection) {
        this.centerQuad = true;
        this.centerQuadX = xDirection;
        this.centerQuadZ = zDirection;
        return this;
    }

    public BoxRenderBuilder centerQuad() {
        return centerQuad(1, 1);
    }

    public BoxRenderBuilder rotate(Quaternion rotation) {
        this.rotation = rotation;
        return this;
    }

    public BoxRenderBuilder rotate(Entity entity) {
        return rotate(RotationUtils.rotation(entity));
    }

    public BoxRenderBuilder rotate(Entity entity, boolean horizontal, boolean vertical) {
        return rotate(RotationUtils.rotation(
                horizontal ? entity.getYaw(0) : 0,
                vertical ? entity.getPitch(0) : 0));
    }

    public BoxRenderBuilder rotate(Entity entity, int horizDir, int vertDir) {
        return rotate(RotationUtils.rotation(
                entity.getYaw(0) * horizDir,
                entity.getPitch(0) * vertDir));
    }

    public BoxRenderBuilder outline(ColorComponents color) {
        this.outline = true;
        this.outlineColor = color;
        return this;
    }

    public BoxRenderBuilder fill(ColorComponents color) {
        this.fill = true;
        this.fillColor = color;
        return this;
    }

    public void draw() {
        prepareDraw();
        applyTransformations();
        if (outline) {
            drawOutline();
        }
        if (fill) {
            drawFill();
        }
        finalizeDraw();
    }

    void prepareDraw() {
        if (centerShapeX || centerShapeY || centerShapeZ) {
            shape = VoxelFactory.center(shape, centerShapeX, centerShapeY, centerShapeZ);
        }
        if (pos == null) {
            pos = Vec3d.ZERO;
        }
        matrixStack.push();
    }

    void applyTransformations() {
        matrixStack.translate(pos.x - cameraPos.x, pos.y - cameraPos.y, pos.z - cameraPos.z);
        if (rotation != null) {
            matrixStack.multiply(rotation);
        }
        if (this.centerQuad) {
            Box bb = shape.getBoundingBox();
            matrixStack.translate(
                    (double)centerQuadX * .5 * bb.getXLength(), 0, (double)centerQuadZ * .5 * bb.getZLength());
        }
    }

    private void drawOutline() {
        BoxRenderer.drawBoxOutlineDirect(matrixStack, immediate.getBuffer(RenderLayers.lines()), shape, outlineColor);
    }

    private void drawFill() {
        BoxRenderer.drawFilledShape(matrixStack, immediate.getBuffer(RenderLayers.fill()), shape, fillColor);
    }

    void finalizeDraw() {
        matrixStack.pop();
    }

}
