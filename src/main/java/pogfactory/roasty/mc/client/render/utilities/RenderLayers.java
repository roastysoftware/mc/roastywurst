package pogfactory.roasty.mc.client.render.utilities;

import bleach.hack.utils.RenderUtils;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.RenderPhase;
import net.minecraft.client.render.VertexFormats;
import org.lwjgl.opengl.GL11;

import java.util.OptionalDouble;

public final class RenderLayers {

    public static RenderLayer fill() {
        return RenderLayer.of("fill_custom", VertexFormats.POSITION_COLOR, 7, 16384, false, false,
                RenderLayer.MultiPhaseParameters.builder()
                        .depthTest(DepthTest.NOT_EQUAL)
                        .layering(Layering.POLYGON_OFFSET)
                        .transparency(Transparency.TRANSLUCENT)
                        .writeMaskState(WriteMaskState.ALL)
                        .cull(Cull.DISABLE)
                        .build(false));
    }

    protected static RenderLayer.MultiPhaseParameters buildLines(double lineWidth) {
        return RenderLayer.MultiPhaseParameters.builder()
                .depthTest(DepthTest.NOT_EQUAL)
                .lineWidth(new RenderPhase.LineWidth(OptionalDouble.of(lineWidth)))
                .layering(Layering.POLYGON_OFFSET)
                .transparency(Transparency.TRANSLUCENT)
                .writeMaskState(WriteMaskState.ALL)
                .cull(Cull.DISABLE)
                .build(false);
    }

    public static RenderLayer lines(double lineWidth) {
        return RenderLayer.of("lines_custom", VertexFormats.POSITION_COLOR, 1, 256,
                buildLines(lineWidth));
    }

    public static RenderLayer lines() {
        return lines(1);
    }

    public static RenderLayer glow() {
        return RenderLayer.of("glow_custom", VertexFormats.POSITION_COLOR, 1, 256,
                RenderLayer.MultiPhaseParameters.builder()
                        .depthTest(DepthTest.NOT_EQUAL)
                        .lineWidth(new RenderPhase.LineWidth(OptionalDouble.empty()))
                        .layering(Layering.POLYGON_OFFSET)
                        .transparency(Transparency.TRANSLUCENT)
                        .writeMaskState(WriteMaskState.ALL)
                        .cull(Cull.DISABLE)
                        .build(false));
    }

    public static final class Transparency {
        public static final RenderPhase.Transparency TRANSLUCENT =
                new RenderPhase.Transparency("translucent", () -> {
            RenderSystem.enableBlend();
            RenderSystem.blendFuncSeparate(
                    GlStateManager.SrcFactor.SRC_ALPHA,
                    GlStateManager.DstFactor.ONE_MINUS_SRC_ALPHA,
                    GlStateManager.SrcFactor.ONE,
                    GlStateManager.DstFactor.ONE_MINUS_SRC_ALPHA);
        }, () -> {
            RenderSystem.disableBlend();
            RenderSystem.defaultBlendFunc();
        });
    }

    public static final class ShadeModel {
        public static final RenderPhase.ShadeModel SMOOTH = new RenderPhase.ShadeModel(true);
        public static final RenderPhase.ShadeModel NON_SMOOTH = new RenderPhase.ShadeModel(false);
    }

    public static final class DepthTest {
        public static final RenderPhase.DepthTest NEVER = new RenderPhase.DepthTest("<", GL11.GL_NEVER);
        public static final RenderPhase.DepthTest LESS_THAN = new RenderPhase.DepthTest("<", GL11.GL_LESS);
        public static final RenderPhase.DepthTest LESS_THAN_OR_EQUAL = new RenderPhase.DepthTest("<=", GL11.GL_LEQUAL);
        public static final RenderPhase.DepthTest EQUAL = new RenderPhase.DepthTest("==", GL11.GL_EQUAL);
        public static final RenderPhase.DepthTest GREATER_THAN_OR_EQUAL = new RenderPhase.DepthTest(">=", GL11.GL_GEQUAL);
        public static final RenderPhase.DepthTest GREATER_THAN = new RenderPhase.DepthTest(">", GL11.GL_GREATER);
        public static final RenderPhase.DepthTest ALWAYS = new RenderPhase.DepthTest("always", GL11.GL_ALWAYS);
        public static final RenderPhase.DepthTest NOT_EQUAL = new RenderPhase.DepthTest("!=", GL11.GL_NOTEQUAL);

    }

    public static final class Layering {
        public static final RenderPhase.Layering NONE = new RenderPhase.Layering("none", () -> {}, () -> {});
        public static final RenderPhase.Layering VIEW_OFFSET_Z =
                new RenderPhase.Layering("view_offset_z_custom", () -> {
            GL11.glPushMatrix();
            GL11.glScalef(0.99975586F, 0.99975586F, 0.99975586F);
        }, GL11::glPopMatrix);
        public static final RenderPhase.Layering POLYGON_OFFSET = new RenderPhase.Layering("polygon_offset_custom", () -> {
            RenderSystem.polygonOffset(-1.0F, -10.0F);
            RenderSystem.enablePolygonOffset();
        }, () -> {
            RenderSystem.polygonOffset(0.0F, 0.0F);
            RenderSystem.disablePolygonOffset();
        });
    }

    public static final class WriteMaskState {
        public static final RenderPhase.WriteMaskState ALL = new RenderPhase.WriteMaskState(true, true);
        public static final RenderPhase.WriteMaskState DEPTH = new RenderPhase.WriteMaskState(false, true);
        public static final RenderPhase.WriteMaskState COLOR = new RenderPhase.WriteMaskState(true, false);
    }

    public static final class Cull {
        public static final RenderPhase.Cull DISABLE = new RenderPhase.Cull(false);
        public static final RenderPhase.Cull ENABLE = new RenderPhase.Cull(true);
    }

}
