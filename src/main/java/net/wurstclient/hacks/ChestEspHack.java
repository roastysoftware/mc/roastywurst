/*
 * Copyright (c) 2014-2021 Wurst-Imperium and contributors.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;


import java.util.ArrayList;

import net.minecraft.entity.Entity;
import org.lwjgl.opengl.GL11;

import net.minecraft.block.BlockState;
import net.minecraft.block.ChestBlock;
import net.minecraft.block.entity.BarrelBlockEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.ChestBlockEntity;
import net.minecraft.block.entity.EnderChestBlockEntity;
import net.minecraft.block.entity.ShulkerBoxBlockEntity;
import net.minecraft.block.entity.TrappedChestBlockEntity;
import net.minecraft.block.enums.ChestType;
import net.minecraft.block.entity.*;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.entity.vehicle.ChestMinecartEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Quaternion;
import net.minecraft.util.shape.VoxelShapes;
import net.wurstclient.Category;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.events.WorldRenderListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.util.BlockUtils;
import org.apache.logging.log4j.LogManager;
import pogfactory.roasty.mc.client.render.BlockBoxRenderBuilder;
import pogfactory.roasty.mc.client.render.BoxRenderBuilder;
import pogfactory.roasty.mc.client.render.BoxRenderer;
import pogfactory.roasty.mc.client.render.EntityBoxRenderBuilder;
import pogfactory.roasty.mc.client.render.collections.VoxelShapePresets;
import pogfactory.roasty.mc.client.render.utilities.VoxelFactory;
import pogfactory.roasty.mc.client.utility.color.ColorComponents;

import java.util.ArrayList;

public class ChestEspHack extends Hack implements UpdateListener, WorldRenderListener {

    private final ArrayList<BlockPos> basicChests = new ArrayList<>();
    private final ArrayList<BlockPos> trappedChests = new ArrayList<>();
    private final ArrayList<BlockPos> enderChests = new ArrayList<>();
    private final ArrayList<BlockPos> shulkerBoxes = new ArrayList<>();
    private final ArrayList<Entity> minecarts = new ArrayList<>();

    public ChestEspHack() {
        super("ChestESP",
                "Highlights nearby chests.\n"
                        + "\u00a7agreen\u00a7r - normal chests & barrels\n"
                        + "\u00a76orange\u00a7r - trapped chests\n"
                        + "\u00a7bcyan\u00a7r - ender chests\n"
                        + "\u00a7dpurple\u00a7r - shulker boxes");

        setCategory(Category.RENDER);
    }

    @Override
    protected void onEnable() {
        EVENTS.add(UpdateListener.class, this);
        EVENTS.add(WorldRenderListener.class, this);
    }

    @Override
    protected void onDisable() {
        EVENTS.remove(UpdateListener.class, this);
        EVENTS.remove(WorldRenderListener.class, this);
    }


    @Override
    public void onUpdate() {
        basicChests.clear();
        trappedChests.clear();
        enderChests.clear();
        shulkerBoxes.clear();

        for (BlockEntity blockEntity : MC.world.blockEntities)
            if (blockEntity instanceof TrappedChestBlockEntity) {
                trappedChests.add(blockEntity.getPos());
            } else if (blockEntity instanceof ChestBlockEntity) {
                basicChests.add(blockEntity.getPos());
            } else if (blockEntity instanceof EnderChestBlockEntity) {
                BlockPos pos = blockEntity.getPos();
                if (!BlockUtils.canBeClicked(pos))
                    continue;

                enderChests.add(pos);
            } else if (blockEntity instanceof ShulkerBoxBlockEntity) {
                BlockPos pos = blockEntity.getPos();
                if (!BlockUtils.canBeClicked(pos))
                    continue;

                shulkerBoxes.add(pos);
            } else if (blockEntity instanceof BarrelBlockEntity) {
                BlockPos pos = blockEntity.getPos();
                if (!BlockUtils.canBeClicked(pos))
                    continue;

                basicChests.add(pos);
            }

        minecarts.clear();
        for (Entity entity : MC.world.getEntities())
            if (entity instanceof ChestMinecartEntity)
                minecarts.add(entity);
    }

    private void renderBox(BlockPos blockPos, WorldRenderEvent event, ColorComponents color) {
        ColorComponents fillColor = new ColorComponents(color.red, color.green, color.blue, color.alpha * 1 / 3f);
        BlockBoxRenderBuilder.build(event).block(blockPos).outline(color).fill(fillColor).draw();
    }

    private void renderBox(ChestMinecartEntity entity, WorldRenderEvent event, ColorComponents color) {
        ColorComponents fillColor = new ColorComponents(color.red, color.green, color.blue, color.alpha * 1 / 3f);
        EntityBoxRenderBuilder.build(event)
                .entity(entity)
                .outline(color)
                .fill(fillColor)
                .draw();
    }

    @Override
    public void onWorldRender(WorldRenderEvent event) {
        try {
            basicChests.forEach(blockPos -> renderBox(blockPos, event, new ColorComponents(.4f, 1f, .4f, .5f)));
            shulkerBoxes.forEach(blockPos -> renderBox(blockPos, event, new ColorComponents(.7f, .4f, 1f, .5f)));
            enderChests.forEach(blockPos -> renderBox(blockPos, event, new ColorComponents(.4f, .6f, 1f, .5f)));
            trappedChests.forEach(blockPos -> renderBox(blockPos, event, new ColorComponents(1f, .4f, .4f, .5f)));
            minecarts.forEach(entity -> renderBox((ChestMinecartEntity) entity, event, new ColorComponents(.5f, .65f, .5f, .5f)));
        } catch (Throwable t) {
            LogManager.getFormatterLogger("ChestEspHack:onWorldRender").error(t);
            t.printStackTrace();
        }
    }

}
