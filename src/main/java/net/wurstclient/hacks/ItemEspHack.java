/*
 * Copyright (c) 2014-2021 Wurst-Imperium and contributors.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;

import java.util.ArrayList;

import net.wurstclient.events.WorldRenderListener;

import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.EnumSetting;
import pogfactory.roasty.mc.client.render.EntityBoxRenderBuilder;
import pogfactory.roasty.mc.client.render.TargetLineRenderBuilder;
import pogfactory.roasty.mc.client.utility.color.ColorComponents;

@SearchTags({"item esp", "ItemTracers", "item tracers"})
public final class ItemEspHack extends Hack implements UpdateListener, WorldRenderListener
{
	private final CheckboxSetting names = new CheckboxSetting("Show item names",
		"Sorry, this is currently broken!\n"
			+ "19w39a changed how nameplates work\n"
			+ "and we haven't figured it out yet.",
		true);
	
	private final EnumSetting<Style> style =
		new EnumSetting<>("Style", Style.values(), Style.GLOW);

	private final ArrayList<ItemEntity> items = new ArrayList<>();
	private final ColorComponents color = new ColorComponents(1, 1, 0, 0.5F);
	
	public ItemEspHack()
	{
		super("ItemESP", "Highlights nearby items.");
		setCategory(Category.RENDER);
		
		addSetting(names);
		addSetting(style);
	}
	
	@Override
	public void onEnable()
	{
		EVENTS.add(UpdateListener.class, this);
		EVENTS.add(WorldRenderListener.class, this);
	}
	
	@Override
	public void onDisable()
	{
		EVENTS.remove(UpdateListener.class, this);
		EVENTS.remove(WorldRenderListener.class, this);
		items.forEach(item -> item.setGlowing(false));
	}
	
	@Override
	public void onUpdate()
	{
		items.clear();
		for(Entity entity : MC.world.getEntities())
			if(entity instanceof ItemEntity) {
				items.add((ItemEntity) entity);
				entity.setGlowing(style.getSelected().glow);
			}
	}

	@Override
	public void onWorldRender(WorldRenderEvent event) {
		items.forEach(item -> {
			if (style.getSelected().lines) {
				TargetLineRenderBuilder.build(event).target(item).color(color).draw();
			}
		});
	}

	private enum Style
	{
		GLOW("Glow only", true, false),
		LINES("Lines only", false, true),
		LINES_AND_GLOW("Lines and glow", true, true);
		
		private final String name;
		private final boolean glow;
		private final boolean lines;
		
		private Style(String name, boolean glow, boolean lines)
		{
			this.name = name;
			this.glow = glow;
			this.lines = lines;
		}
		
		@Override
		public String toString()
		{
			return name;
		}
	}

}
