/*
 * Copyright (C) 2014 - 2020 | Alexander01998 | All rights reserved.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;

import net.minecraft.client.network.PlayerListEntry;
import net.minecraft.client.network.ServerInfo;
import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.SliderSetting;

import java.util.ArrayList;
import java.util.Collection;

@SearchTags({"greet everyone"})
public final class GreetEveryoneHack extends Hack implements UpdateListener {

	private final SliderSetting intervalMs = new SliderSetting("Message interval",
			"How long to wait between messages\n\n",
			600, 0, 2000, 200, SliderSetting.ValueDisplay.INTEGER);

	private ArrayList<String> alreadyGreetedPlayers;
	private String myName;
	private long lastMessageTime;
	private ServerInfo server;

	public GreetEveryoneHack() {
		super("GreetEveryone", "Greets everyone when you join the server, and when others join.");
		setCategory(Category.CHAT);
		addSetting(intervalMs);
	}

	@Override
	public void onEnable() {
		EVENTS.add(UpdateListener.class, this);
		myName = MC.player.getName().asString();
		lastMessageTime = System.currentTimeMillis();
		alreadyGreetedPlayers = new ArrayList<>();
	}

	@Override
	public void onDisable() {
		EVENTS.remove(UpdateListener.class, this);
		alreadyGreetedPlayers = null;
		myName = null;
	}

	@Override
	public void onUpdate() {
		if (MC.getCurrentServerEntry() == null ||
				server != null && !MC.getCurrentServerEntry().address.equals(server.address)) {
			if (myName != null) {
				alreadyGreetedPlayers = new ArrayList<>();
				myName = null;
			}
			return;
		}
		if (lastMessageTime + intervalMs.getValueI() > System.currentTimeMillis()) {
			return;
		}
		if (alreadyGreetedPlayers == null ||
			MC.getNetworkHandler() == null ||
			MC.getNetworkHandler().getPlayerList() == null) {
			return;
		}
		if (server == null) {
			server = MC.getCurrentServerEntry();
		}

		new Thread(() -> {
			synchronized (alreadyGreetedPlayers) {
				try {
					Collection<PlayerListEntry> playerList = MC.getNetworkHandler().getPlayerList();
					for (PlayerListEntry player : playerList) {
						if (player == null) continue;
						String playerName = player.getProfile().getName();
						if (playerName.equals(myName)) {
							continue;
						}
						if (alreadyGreetedPlayers.contains(playerName)) {
							continue;
						}
						if (MC.player == null) continue;
						System.out.println("Hi " + playerName + "!");
						MC.player.sendChatMessage("Hi " + playerName + "!");
						alreadyGreetedPlayers.add(playerName);
						break;
					}

					String playerToRemove = null;
					for (String greetedPlayerName : alreadyGreetedPlayers) {
						boolean found = false;
						for (PlayerListEntry player : playerList) {
							if (player == null) continue;
							String playerName = player.getProfile().getName();
							if (playerName.equals(greetedPlayerName)) {
								found = true;
								break;
							}
						}
						if (!found) {
							MC.player.sendChatMessage("Bye " + greetedPlayerName + "!");
							playerToRemove = greetedPlayerName;
							break;
						}
					}
					if (playerToRemove != null) {
						alreadyGreetedPlayers.remove(playerToRemove);
					}

					lastMessageTime = System.currentTimeMillis();
				} catch (Throwable t) {
					System.err.println("Oh snap, something went wrong!");
					System.err.println(t.getMessage());
				}
			}
		}).start();
	}
}
