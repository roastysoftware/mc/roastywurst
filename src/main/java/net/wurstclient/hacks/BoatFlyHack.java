/*
 * Copyright (c) 2014-2021 Wurst-Imperium and contributors.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;

import net.minecraft.entity.Entity;
import net.minecraft.network.Packet;
import net.minecraft.network.packet.c2s.play.PlayerMoveC2SPacket;
import net.minecraft.network.packet.c2s.play.VehicleMoveC2SPacket;
import net.minecraft.network.packet.s2c.play.EntityAttachS2CPacket;
import net.minecraft.network.packet.s2c.play.EntityPositionS2CPacket;
import net.minecraft.network.packet.s2c.play.EntityVelocityUpdateS2CPacket;
import net.minecraft.network.packet.s2c.play.PlayerPositionLookS2CPacket;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.PacketInputListener;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.SliderSetting;

@SearchTags({"BoatFlight", "boat fly", "boat flight"})
public final class BoatFlyHack extends Hack implements UpdateListener, PacketInputListener
{

	public final SliderSetting speed =
			new SliderSetting("Speed", 0.3, 0.05, 1.0, 0.05, SliderSetting.ValueDisplay.DECIMAL);

	public final CheckboxSetting groundPacket = new CheckboxSetting(
			"Ground packet", "Sends packets to the server claiming that you are on the ground", false);

	public final CheckboxSetting skipVelocity = new CheckboxSetting(
			"Skip velocity", "Teleports you without velocity", false);

	public final CheckboxSetting skipPosition = new CheckboxSetting(
			"Skip position", "Only sets a velocity without setting your position", false);

	public final CheckboxSetting noGravity = new CheckboxSetting(
			"No gravity", false);

	public final CheckboxSetting applyToPlayer = new CheckboxSetting(
			"Apply to player", "Applies boat movements to the player", false);

	public final CheckboxSetting forceRemount = new CheckboxSetting(
			"Force remount", "Forces the player to always be riding the boat", false);

	public final CheckboxSetting speedHack = new CheckboxSetting(
			"Speed hack", "Makes you go faster", false);

	public final CheckboxSetting ignoreServerPosition = new CheckboxSetting(
			"Ignore server pos", "Ignores server packets which change your position", false);

	private boolean hadVehicle = false;
	private double playerY;
	private Entity vehicle;

	public BoatFlyHack()
	{
		super("BoatFly", "Allows you to fly with boats");
		setCategory(Category.MOVEMENT);
		addSetting(speed);
		addSetting(groundPacket);
		addSetting(skipVelocity);
		addSetting(skipPosition);
		addSetting(noGravity);
		addSetting(applyToPlayer);
		addSetting(forceRemount);
		addSetting(speedHack);
		addSetting(ignoreServerPosition);
	}
	
	@Override
	public void onEnable()
	{
		EVENTS.add(UpdateListener.class, this);
		EVENTS.add(PacketInputListener.class, this);
	}
	
	@Override
	public void onDisable()
	{
		EVENTS.remove(UpdateListener.class, this);
		EVENTS.remove(PacketInputListener.class, this);
	}
	
	@Override
	public void onUpdate()
	{
		// check if riding
		if(!MC.player.hasVehicle()) {
			if (hadVehicle) {
				if (forceRemount.isChecked() && vehicle != null) {
					MC.player.startRiding(vehicle, true);
				} else {
					hadVehicle = false;
					vehicle = null;
				}
			}
			return;
		}

		if (!hadVehicle) {
			playerY = MC.player.getY();
			vehicle = MC.player.getVehicle();
		}
		hadVehicle = true;
		
		// fly
		Vec3d velocity = vehicle.getVelocity();
		double motionY = MC.options.keyJump.isPressed() ? speed.getValueF() : 0;
		double newVehicleVelocityX = velocity.x;
		double newVehicleVelocityZ = velocity.z;

		if (speedHack.isChecked()) {
			newVehicleVelocityX = 6;
			newVehicleVelocityZ = 6;
		}

		Vec3d newVehicleVelocity = new Vec3d(newVehicleVelocityX, motionY, newVehicleVelocityZ);

		if (!skipVelocity.isChecked()) {
			vehicle.setVelocity(newVehicleVelocity);
			if (applyToPlayer.isChecked()) {
				MC.player.setVelocity(newVehicleVelocity);
			}
		}
		if (!skipPosition.isChecked()) {
			vehicle.setPos(vehicle.getX(), vehicle.getY() + motionY, vehicle.getZ());
			if (applyToPlayer.isChecked()) {
				MC.player.setPos(vehicle.getX(), vehicle.getY() + motionY, vehicle.getZ());
			}
		}
		if (noGravity.isChecked()) {
			vehicle.setNoGravity(true);
		}

		if (groundPacket.isChecked()) {
			MC.player.networkHandler.sendPacket(
					new PlayerMoveC2SPacket.PositionOnly(vehicle.getX(), playerY, vehicle.getZ(), true));
		}
	}

	@Override
	public void onReceivedPacket(PacketInputEvent event) {
		if (MC.player == null || !MC.player.hasVehicle()) {
			return;
		}
		Packet<?> packet = event.getPacket();
		if (packet instanceof PlayerPositionLookS2CPacket ||
				packet instanceof EntityPositionS2CPacket ||
				packet instanceof EntityAttachS2CPacket ||
				packet instanceof EntityVelocityUpdateS2CPacket) {
			if (ignoreServerPosition.isChecked()) {
				event.cancel();
			}
		}
	}
}
