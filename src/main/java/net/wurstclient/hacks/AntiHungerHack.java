package net.wurstclient.hacks;

import bleach.hack.utils.FabricReflect;
import net.minecraft.network.Packet;
import net.minecraft.network.packet.c2s.play.PlayerMoveC2SPacket;
import net.wurstclient.Category;
import net.wurstclient.events.PacketOutputListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;

public class AntiHungerHack extends Hack implements PacketOutputListener {

    private static final CheckboxSetting relaxed = new CheckboxSetting(
            "Relaxed", "Only activates every other ticks, might fix getting fly kicked", false);

    private boolean bool = false;

    public AntiHungerHack() {
        super("AntiHunger", "Minimizes the amount of hunger you use");
        setCategory(Category.OTHER);
        addSetting(relaxed);
    }

    @Override
    protected void onEnable() {
        EVENTS.add(PacketOutputListener.class, this);
    }

    @Override
    protected void onDisable() {
        EVENTS.remove(PacketOutputListener.class, this);
    }

    @Override
    public void onSentPacket(PacketOutputEvent event) {
        Packet<?> packet = event.getPacket();
        if (packet instanceof PlayerMoveC2SPacket) {
            if (MC.player.getVelocity().y != 0 && !MC.options.keyJump.isPressed() &&
                    (!bool || !relaxed.isChecked())) {
                //if (((PlayerMoveC2SPacket) event.getPacket()).isOnGround()) event.setCancelled(true);
                boolean onGround = MC.player.fallDistance >= 0.1f;
                MC.player.setOnGround(onGround);
                FabricReflect.writeField(packet, onGround, "field_12891", "onGround");
                bool = true;
            } else {
                bool = false;
            }
        }
    }
}
