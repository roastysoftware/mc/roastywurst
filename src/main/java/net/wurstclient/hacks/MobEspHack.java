/*
 * Copyright (c) 2014-2021 Wurst-Imperium and contributors.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import net.wurstclient.events.WorldRenderListener;
import org.lwjgl.opengl.GL11;

import net.minecraft.entity.mob.MobEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.CameraTransformViewBobbingListener;
import net.wurstclient.events.RenderListener;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.EnumSetting;
import net.wurstclient.util.RenderUtils;
import net.wurstclient.util.RotationUtils;
import pogfactory.roasty.mc.client.render.BoxRenderBuilder;
import pogfactory.roasty.mc.client.render.BoxRenderer;
import pogfactory.roasty.mc.client.render.EntityBoxRenderBuilder;
import pogfactory.roasty.mc.client.render.TargetLineRenderBuilder;
import pogfactory.roasty.mc.client.render.collections.VoxelShapePresets;
import pogfactory.roasty.mc.client.utility.color.ColorComponents;

@SearchTags({"mob esp", "MobTracers", "mob tracers"})
public final class MobEspHack extends Hack implements UpdateListener, WorldRenderListener
{
	private final EnumSetting<Style> style =
		new EnumSetting<>("Style", Style.values(), Style.BOXES);
	
	private final CheckboxSetting filterInvisible = new CheckboxSetting(
		"Filter invisible", "Won't show invisible mobs.", false);

	private final ArrayList<MobEntity> mobs = new ArrayList<>();
	
	public MobEspHack()
	{
		super("MobESP", "Highlights nearby mobs.");
		setCategory(Category.RENDER);
		addSetting(style);
		addSetting(filterInvisible);
	}
	
	@Override
	public void onEnable()
	{
		EVENTS.add(UpdateListener.class, this);
		EVENTS.add(WorldRenderListener.class, this);
	}
	
	@Override
	public void onDisable()
	{
		EVENTS.remove(UpdateListener.class, this);
		EVENTS.remove(WorldRenderListener.class, this);
	}
	
	@Override
	public void onUpdate()
	{
		mobs.clear();
		
		Stream<MobEntity> stream =
			StreamSupport.stream(MC.world.getEntities().spliterator(), false)
				.filter(e -> e instanceof MobEntity).map(e -> (MobEntity)e)
				.filter(e -> !e.removed && e.getHealth() > 0);
		
		if(filterInvisible.isChecked())
			stream = stream.filter(e -> !e.isInvisible());
		
		mobs.addAll(stream.collect(Collectors.toList()));
	}

	@Override
	public void onWorldRender(WorldRenderEvent event) {
		boolean renderBoxes = style.getSelected().boxes;
		boolean renderTracers = style.getSelected().lines;

		for(MobEntity e : mobs) {
			float f = Math.min(MC.player.distanceTo(e), 20) / 20;
			ColorComponents color = new ColorComponents(1 - f, f, 0, 0.5F);
			if (renderBoxes) {
				EntityBoxRenderBuilder.build(event).entity(e).outline(color).draw();
			}
			if (renderTracers) {
				TargetLineRenderBuilder.build(event).target(e).color(color).draw();
			}
		}
	}

	private enum Style
	{
		BOXES("Boxes only", true, false),
		LINES("Lines only", false, true),
		LINES_AND_BOXES("Lines and boxes", true, true);
		
		private final String name;
		private final boolean boxes;
		private final boolean lines;
		
		private Style(String name, boolean boxes, boolean lines)
		{
			this.name = name;
			this.boxes = boxes;
			this.lines = lines;
		}
		
		@Override
		public String toString()
		{
			return name;
		}
	}
	

}
