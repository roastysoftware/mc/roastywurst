/*
 * This file is part of the BleachHack distribution (https://github.com/BleachDrinker420/bleachhack-1.14/).
 * Copyright (c) 2019 Bleach.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wurstclient.hacks;

import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.ingame.InventoryScreen;
import net.minecraft.item.ElytraItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.screen.slot.SlotActionType;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;

public class ElytraReplaceHack extends Hack implements UpdateListener {

    private boolean jump = false;

    public ElytraReplaceHack() {
        super("ElytraReplace", "Automatically replaces broken elytra and continues flying");
    }

    @Override
    public void onEnable() {
        EVENTS.add(UpdateListener.class, this);
    }

    @Override
    public void onDisable() {
        EVENTS.remove(UpdateListener.class, this);
    }

    @Override
    public void onUpdate() {
        if((MC.currentScreen instanceof HandledScreen && !(MC.currentScreen instanceof InventoryScreen)) && MC.currentScreen != null) return;

        int chestSlot = 38;
        ItemStack chest = MC.player.inventory.getStack(chestSlot);
        if (chest.getItem() instanceof ElytraItem && chest.getDamage() == (Items.ELYTRA.getMaxDamage() - 1)) {
            // search inventory for elytra

            Integer elytraSlot = null;
            for (int slot = 0; slot < 36; slot++) {
                ItemStack stack = MC.player.inventory.getStack(slot);
                if (!stack.isEmpty() && stack.getItem() instanceof ElytraItem && stack.getDamage() != (Items.ELYTRA.getMaxDamage() - 1)) {
                    elytraSlot = slot;
                    break;
                }
            }

            if (elytraSlot == null) {
                return;
            }

            MC.interactionManager.clickSlot(MC.player.currentScreenHandler.syncId, 6, 0, SlotActionType.PICKUP, MC.player);
            MC.interactionManager.clickSlot(MC.player.currentScreenHandler.syncId, elytraSlot < 9 ? (elytraSlot + 36) : (elytraSlot), 0, SlotActionType.PICKUP, MC.player);
            MC.interactionManager.clickSlot(MC.player.currentScreenHandler.syncId, 6, 0, SlotActionType.PICKUP, MC.player);

            MC.options.keyJump.setPressed(true);  // Make them fly again
            jump = true;
        } else if (jump) {
            MC.options.keyJump.setPressed(false); // Make them fly again
            jump = false;
        }
    }
}

