package net.wurstclient.hacks;

import net.minecraft.network.packet.c2s.play.PlayerMoveC2SPacket;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.EnumSetting;
import net.wurstclient.settings.SliderSetting;
import net.wurstclient.util.WorldUtils;

@SearchTags({"bleach flight", "fly hack"})
public class BleachFlightHack extends Hack implements UpdateListener {

    private SliderSetting speedSlider = new SliderSetting(
            "Speed", "Flight speed",
            1, 0, 5, 0.2,
            SliderSetting.ValueDisplay.DECIMAL);

    private EnumSetting<FlightMode> flightModeSetting = new EnumSetting<>(
            "Mode", "Flight mode", FlightMode.values(), FlightMode.NORMAL);

    private EnumSetting<AntiKickMode> antiKickModeSetting = new EnumSetting<>(
            "AntiKick", "AntiKick mode", AntiKickMode.values(), AntiKickMode.OFF);

    public BleachFlightHack() {
        super("BleachFlight", "Allows you to fly like in Bleach");
        setCategory(Category.MOVEMENT);
        addSetting(speedSlider);
        addSetting(flightModeSetting);
        addSetting(antiKickModeSetting);
    }

    @Override
    public void onDisable() {
        EVENTS.remove(UpdateListener.class, this);

        if (!MC.player.abilities.creativeMode) MC.player.abilities.allowFlying = false;
        MC.player.abilities.flying = false;
    }

    @Override
    protected void onEnable() {
        WURST.getHax().flightHack.setEnabled(false);
        WURST.getHax().jetpackHack.setEnabled(false);

        EVENTS.add(UpdateListener.class, this);
    }

    public void onUpdate() {
        float speed = speedSlider.getValueF();
        FlightMode flightMode = flightModeSetting.getSelected();
        AntiKickMode antiKickMode = antiKickModeSetting.getSelected();

        if (MC.player.age % 20 == 0 && antiKickMode == AntiKickMode.PACKET && flightMode != FlightMode.JETPACK) {
            MC.player.networkHandler.sendPacket(new PlayerMoveC2SPacket.PositionOnly(MC.player.getX(), MC.player.getY() - 0.06, MC.player.getZ(), false));
            MC.player.networkHandler.sendPacket(new PlayerMoveC2SPacket.PositionOnly(MC.player.getX(), MC.player.getZ() + 0.06, MC.player.getZ(), false));
        }

        if (flightMode == FlightMode.NORMAL) {
            MC.player.abilities.setFlySpeed(speed / 10);
            MC.player.abilities.allowFlying = true;
            MC.player.abilities.flying = true;
            if (MC.options.keySneak.isPressed()) {
                if (WURST.getHax().noFallHack.isEnabled()) WURST.getHax().noFallHack.preventFallDamage();
            }
        } else if (flightMode == FlightMode.STATIC) {
            if (antiKickMode == AntiKickMode.OFF || antiKickMode == AntiKickMode.PACKET) MC.player.setVelocity(0, 0, 0);
            else if (antiKickMode == AntiKickMode.FALL && WorldUtils.NONSOLID_BLOCKS.contains(MC.world.getBlockState(new BlockPos(MC.player.getPos().getX(), MC.player.getPos().getY() - 0.069, MC.player.getPos().getZ())).getBlock()))
                MC.player.setVelocity(0, MC.player.age % 20 == 0 ? -0.069 : 0, 0);
            else if (antiKickMode == AntiKickMode.BOB)
                MC.player.setVelocity(0, MC.player.age % 40 == 0 ? (WorldUtils.NONSOLID_BLOCKS.contains(MC.world.getBlockState(new BlockPos(MC.player.getPos().getX(), MC.player.getPos().getY() + 1.15, MC.player.getPos().getZ())).getBlock()) ? 0.15 : 0) : MC.player.age % 20 == 0 ? (WorldUtils.NONSOLID_BLOCKS.contains(MC.world.getBlockState(new BlockPos(MC.player.getPos().getX(), MC.player.getPos().getY() - 0.15, MC.player.getPos().getZ())).getBlock()) ? -0.15 : 0) : 0, 0);
            Vec3d forward = new Vec3d(0, 0, speed).rotateY(-(float) Math.toRadians(MC.player.yaw));
            Vec3d strafe = forward.rotateY((float) Math.toRadians(90));

            if (MC.options.keyJump.isPressed()) MC.player.setVelocity(MC.player.getVelocity().add(0, speed, 0));
            if (MC.options.keySneak.isPressed()) {
                MC.player.setVelocity(MC.player.getVelocity().add(0, -speed, 0));
                if (WURST.getHax().noFallHack.isEnabled()) WURST.getHax().noFallHack.preventFallDamage();
            }
            if (MC.options.keyBack.isPressed())
                MC.player.setVelocity(MC.player.getVelocity().add(-forward.x, 0, -forward.z));
            if (MC.options.keyForward.isPressed())
                MC.player.setVelocity(MC.player.getVelocity().add(forward.x, 0, forward.z));
            if (MC.options.keyLeft.isPressed())
                MC.player.setVelocity(MC.player.getVelocity().add(strafe.x, 0, strafe.z));
            if (MC.options.keyRight.isPressed())
                MC.player.setVelocity(MC.player.getVelocity().add(-strafe.x, 0, -strafe.z));

        } else if (flightMode == FlightMode.JETPACK) {
            if (!MC.options.keyJump.isPressed()) return;
            MC.player.setVelocity(MC.player.getVelocity().x, speed / 3, MC.player.getVelocity().z);
        }
    }


    private enum FlightMode {
        NORMAL("Normal"),
        STATIC("Static"),
        JETPACK("Jetpack");

        private final String name;

        private FlightMode(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }


    private enum AntiKickMode {
        OFF("Off"),
        FALL("Fall"),
        BOB("Bob"),
        PACKET("Packet");

        private final String name;

        private AntiKickMode(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
