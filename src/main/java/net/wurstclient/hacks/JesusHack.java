/*
 * Copyright (c) 2014-2021 Wurst-Imperium and contributors.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;

import bleach.hack.utils.WorldUtils;
import net.minecraft.block.Material;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.network.Packet;
import net.minecraft.network.packet.c2s.play.PlayerActionC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayerMoveC2SPacket;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShape;
import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.PacketOutputListener;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.SliderSetting;
import net.wurstclient.util.BlockUtils;
import net.wurstclient.util.ChatUtils;

import java.util.ArrayList;
import java.util.stream.Collectors;

@SearchTags({"WaterWalking", "water walking"})
public final class JesusHack extends Hack
        implements UpdateListener, PacketOutputListener {

    private final CheckboxSetting bypass =
            new CheckboxSetting("NoCheat+ bypass",
                    "Bypasses NoCheat+ but slows down your movement.", false);

    private final CheckboxSetting secretSauce =
            new CheckboxSetting("Secret sauce", true);

    private final SliderSetting speedSlider = new SliderSetting(
            "Speed",
            "Only works with secret sauce", 1, 0.1, 2.5, 0.05, SliderSetting.ValueDisplay.DECIMAL);

    private final CheckboxSetting swim =
            new CheckboxSetting("Swim", "Instead of walking", false);

    private int tickTimer = 10;
    private int packetTimer = 0;

    private Vec3d lastVelocity = new Vec3d(Double.MIN_VALUE, Double.MIN_VALUE, Double.MIN_VALUE);

    public JesusHack() {
        super("Jesus", "Allows you to walk on water.\n"
                + "Jesus used this hack ~2000 years ago.");
        setCategory(Category.MOVEMENT);
        addSetting(bypass);
        addSetting(secretSauce);
        addSetting(speedSlider);
        addSetting(swim);
    }

    @Override
    public void onEnable() {
        EVENTS.add(UpdateListener.class, this);
        EVENTS.add(PacketOutputListener.class, this);
    }

    @Override
    public void onDisable() {
        EVENTS.remove(UpdateListener.class, this);
        EVENTS.remove(PacketOutputListener.class, this);
    }

    private boolean isFluidAt(Entity entity, double distance) {
        return WorldUtils.isFluid(new BlockPos(entity.getPos().add(0, distance, 0)));
    }

    @Override
    public void onUpdate() {
        ClientPlayerEntity player = MC.player;
        if (player == null) return;

        // check if sneaking
        if (player.isSneaking())
            return;


        if (secretSauce.isChecked()) {
            if (!player.isTouchingWater() || MC.options.keyJump.isPressed()) return;
            if (WorldUtils.doesBoxTouchSolidBlock(player.getBoundingBox())) return;
            if (player.fallDistance >= 1) return;

            // optimum speed
            double walkSpeed = speedSlider.getValue();

            double playerVeloX = player.getVelocity().x;
            double playerVeloZ = player.getVelocity().z;
            double velocityX = playerVeloX;
            double velocityZ = playerVeloZ;

            if (Math.abs(lastVelocity.x - playerVeloX) + Math.abs(lastVelocity.z - playerVeloZ) < 0.001) {
                // Now that we reached equilibrium, we can go faster
                lastVelocity = new Vec3d(lastVelocity.x * walkSpeed, lastVelocity.y, lastVelocity.z * walkSpeed);
                velocityX = lastVelocity.x;
                velocityZ = lastVelocity.z;
            } else {
                lastVelocity = player.getVelocity();
            }

            if (isFluidAt(player, 0.25) || player.isSubmergedInWater()) {
                // player is in water with at least feet covered, move up quickly
                player.setVelocity(velocityX, 0.15, velocityZ);
            } else if (isFluidAt(player, 0.12)) {
                // Player's feet are partially covered in water, stay there
                player.setVelocity(velocityX, 0, velocityZ);
            } else if (isFluidAt(player, -0.04)) {
                // Player is not in water enough, move down
                player.setVelocity(velocityX, -0.06, velocityZ);
            } else if (isOverLiquid()) {
                player.setVelocity(velocityX, -0.1, velocityZ);
            }

            if (swim.isChecked()) {
                if (!player.horizontalCollision) {
                    player.setSprinting(true);
                    player.setSwimming(true);
                }
            }

            if (bypass.isChecked()) bypass.setChecked(false);
        } else {
			// move up in water
			if (player.isTouchingWater()) {
				Vec3d velocity = player.getVelocity();
				player.setVelocity(velocity.x, 0.11, velocity.z);
				tickTimer = 0;
				return;
			}

			// simulate jumping out of water
			Vec3d velocity = player.getVelocity();
			if (tickTimer == 0)
				player.setVelocity(velocity.x, 0.30, velocity.z);
			else if (tickTimer == 1)
				player.setVelocity(velocity.x, 0, velocity.z);

			// update timer
			tickTimer++;
		}
    }

    @Override
    public void onSentPacket(PacketOutputEvent event) {
        if (secretSauce.isChecked()) return;
        // check packet type
        if (!(event.getPacket() instanceof PlayerMoveC2SPacket))
            return;

        PlayerMoveC2SPacket packet = (PlayerMoveC2SPacket) event.getPacket();

        // check if packet contains a position
        if (!(packet instanceof PlayerMoveC2SPacket.PositionOnly
                || packet instanceof PlayerMoveC2SPacket.Both))
            return;

        // check inWater
        if (MC.player.isTouchingWater())
            return;

        // check fall distance
        if (MC.player.fallDistance > 3F)
            return;

        if (!isOverLiquid())
            return;

        // if not actually moving, cancel packet
        if (MC.player.input == null) {
            event.cancel();
            return;
        }

        // wait for timer
        packetTimer++;
        if (packetTimer < 4)
            return;

        // cancel old packet
        event.cancel();

        // get position
        double x = packet.getX(0);
        double y = packet.getY(0);
        double z = packet.getZ(0);

        // offset y
        if (bypass.isChecked() && MC.player.age % 2 == 0)
            y -= 0.05;
        else
            y += 0.05;

        // create new packet
        Packet<?> newPacket;
        if (packet instanceof PlayerMoveC2SPacket.PositionOnly)
            newPacket = new PlayerMoveC2SPacket.PositionOnly(x, y, z, true);
        else
            newPacket = new PlayerMoveC2SPacket.Both(x, y, z, packet.getYaw(0),
                    packet.getPitch(0), true);

        // send new packet
        MC.player.networkHandler.getConnection().send(newPacket);
    }

    public boolean isOverLiquid() {
        boolean foundLiquid = false;
        boolean foundSolid = false;

        // check collision boxes below player
        ArrayList<Box> blockCollisions = MC.world
                .getBlockCollisions(MC.player,
                        MC.player.getBoundingBox().offset(0, -0.5, 0))
                .map(VoxelShape::getBoundingBox)
                .collect(Collectors.toCollection(ArrayList::new));

        for (Box bb : blockCollisions) {
            BlockPos pos = new BlockPos(bb.getCenter());
            Material material = BlockUtils.getState(pos).getMaterial();

            if (material == Material.WATER || material == Material.LAVA)
                foundLiquid = true;
            else if (material != Material.AIR)
                foundSolid = true;
        }

        return foundLiquid && !foundSolid;
    }

    // This will be called from FluidBlockMixin to determine whether the ground is solid
    public boolean shouldBeSolid() {
        return isEnabled() && !secretSauce.isChecked() && MC.player != null && MC.player.fallDistance <= 3
                && !MC.options.keySneak.isPressed() && !MC.player.isTouchingWater();
    }
}
