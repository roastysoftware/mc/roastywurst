/*
 * Copyright (c) 2014-2021 Wurst-Imperium and contributors.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;

import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.SliderSetting;

@SearchTags({"jet pack"})
public final class JetpackHack extends Hack implements UpdateListener
{

	public final SliderSetting delay =
			new SliderSetting("Jump delay", 0, 0, 2000, 100, SliderSetting.ValueDisplay.INTEGER);


	private boolean coolDown = false;

	public JetpackHack()
	{
		super("Jetpack", "Allows you to fly as if you had a jetpack.\n\n"
			+ "\u00a7c\u00a7lWARNING:\u00a7r You will take fall damage if you don't use NoFall.");
		
		setCategory(Category.MOVEMENT);
		addSetting(delay);
	}
	
	@Override
	public void onEnable()
	{
		WURST.getHax().flightHack.setEnabled(false);
		WURST.getHax().bleachFlightHack.setEnabled(false);
		EVENTS.add(UpdateListener.class, this);
	}
	
	@Override
	public void onDisable()
	{
		EVENTS.remove(UpdateListener.class, this);
	}
	
	@Override
	public void onUpdate()
	{
		if(MC.options.keyJump.isPressed() && !coolDown) {
			MC.player.jump();
			int delayMs = delay.getValueI();
			if (delayMs > 0) {
				coolDown = true;
				new Thread(() -> {
					try {
						Thread.sleep(delayMs);
					} catch (InterruptedException e) {
						// whatever
					}
					coolDown = false;
				}).start();
				MC.options.keyJump.setPressed(false);
			}
		}
	}
}
