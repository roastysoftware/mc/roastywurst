/*
 * This file is part of the BleachHack distribution (https://github.com/BleachDrinker420/bleachhack-1.14/).
 * Copyright (c) 2019 Bleach.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wurstclient.hacks;

import bleach.hack.utils.EntityUtils;
import bleach.hack.utils.RenderUtils;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.decoration.EndCrystalEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.entity.vehicle.BoatEntity;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.Category;
import net.wurstclient.events.RenderListener;
import net.wurstclient.events.WorldRenderListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.SliderSetting;
import pogfactory.roasty.mc.client.render.TargetLineRenderBuilder;
import pogfactory.roasty.mc.client.utility.color.ColorComponents;

public class TracersHack extends Hack implements WorldRenderListener {

    private final CheckboxSetting mobs = new CheckboxSetting("Mobs", false);
    private final CheckboxSetting animals = new CheckboxSetting("Animals", false);
    private final CheckboxSetting items = new CheckboxSetting("Items", false);
    private final CheckboxSetting crystals = new CheckboxSetting("Crystals", false);
    private final CheckboxSetting vehicles = new CheckboxSetting("Vehicles", false);
    private final SliderSetting thickness =
            new SliderSetting("Thickness", 1.5, 0.1, 4, 0.1, SliderSetting.ValueDisplay.DECIMAL);


    public TracersHack() {
        super("Tracers", "Shows lines to entities you select");
        setCategory(Category.RENDER);
        addSetting(mobs);
        addSetting(animals);
        addSetting(items);
        addSetting(crystals);
        addSetting(vehicles);
        addSetting(thickness);
    }

    @Override
    protected void onEnable() {
        EVENTS.add(WorldRenderListener.class, this);
    }

    @Override
    protected void onDisable() {
        EVENTS.remove(WorldRenderListener.class, this);
    }

    @Override
    public void onWorldRender(WorldRenderEvent event) {
        final double thickness = this.thickness.getValue();
        MC.world.getEntities().forEach(entity -> {
            TargetLineRenderBuilder builder =
                    new TargetLineRenderBuilder(event)
                    .strokeWidth(thickness)
                    .target(entity);
            if (entity instanceof Monster && mobs.isChecked()) {
                builder.color(new ColorComponents(0f, 0f, 0f));
            } else if (EntityUtils.isAnimal(entity) && animals.isChecked()) {
                builder.color(new ColorComponents(0f, 1f, 0f));
            } else if (entity instanceof ItemEntity && items.isChecked()) {
                builder.color(new ColorComponents(1f, 0.7f, 0f));
            } else if (entity instanceof EndCrystalEntity && crystals.isChecked()) {
                builder.color(new ColorComponents(1f, 0f, 1f));
            } else if ((entity instanceof BoatEntity || entity instanceof AbstractMinecartEntity) && vehicles.isChecked()) {
                builder.color(new ColorComponents(0.5f, 0.5f, 0.5f));
            } else {
                return;
            }
            builder.draw();
        });
    }
}
