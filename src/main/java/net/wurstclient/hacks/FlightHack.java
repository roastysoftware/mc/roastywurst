/*
 * Copyright (c) 2014-2021 Wurst-Imperium and contributors.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;

import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.IsPlayerInWaterListener;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.SliderSetting;
import net.wurstclient.settings.SliderSetting.ValueDisplay;

@SearchTags({"FlyHack", "fly hack", "flying"})
public final class FlightHack extends Hack
	implements UpdateListener, IsPlayerInWaterListener
{

	public final SliderSetting speed =
		new SliderSetting("Speed", 1, 0.05, 5, 0.05, ValueDisplay.DECIMAL);

	private final CheckboxSetting bypassKick =
			new CheckboxSetting("Bypass kick", false);

	private long lastBypass = System.currentTimeMillis();
	private BypassStep bypassStep = BypassStep.BEFORE_BYPASS;
	private boolean wasInAir = false;

	public FlightHack()
	{
		super("Flight",
			"Allows you to you fly.\n\n" + "\u00a7c\u00a7lWARNING:\u00a7r"
				+ " You will take fall damage if you don't use NoFall.");
		setCategory(Category.MOVEMENT);
		addSetting(speed);
		addSetting(bypassKick);
	}
	
	@Override
	public void onEnable()
	{
		WURST.getHax().jetpackHack.setEnabled(false);
		WURST.getHax().bleachFlightHack.setEnabled(false);
		
		EVENTS.add(UpdateListener.class, this);
		EVENTS.add(IsPlayerInWaterListener.class, this);
	}
	
	@Override
	public void onDisable()
	{
		EVENTS.remove(UpdateListener.class, this);
		EVENTS.remove(IsPlayerInWaterListener.class, this);
	}
	
	@Override
	public void onUpdate()
	{
		ClientPlayerEntity player = MC.player;

		boolean isInAir = isInAir(player);
		if (!wasInAir && isInAir) {
			lastBypass = System.currentTimeMillis();
		}
		
		player.abilities.flying = false;
		player.flyingSpeed = speed.getValueF();
		
		player.setVelocity(0, 0, 0);
		Vec3d velocity = player.getVelocity();

		if (MC.options.keySneak.isPressed()) {
			player.setVelocity(velocity.subtract(0, speed.getValue(), 0));
		}

		if (MC.options.keyJump.isPressed()) {
			player.setVelocity(velocity.add(0, speed.getValue(), 0));
		}

		if (bypassKick.isChecked() && (
				bypassStep != BypassStep.BEFORE_BYPASS ||
				(isInAir && lastBypass + 3600 < System.currentTimeMillis())
		)) {
			bypassKick(player);
			lastBypass = System.currentTimeMillis();
		}

		wasInAir = isInAir;
	}

	private void bypassKick(ClientPlayerEntity player) {
		player.setVelocity(0, 0, 0);
		switch (bypassStep) {
			case BEFORE_BYPASS:
				bypassStep = BypassStep.MOVE_DOWN;
				break;
			case MOVE_DOWN:
				player.setVelocity(0, -1.5, 0);
				bypassStep = BypassStep.WAIT;
				break;
			case WAIT:
				bypassStep = BypassStep.MOVE_UP;
				break;
			case MOVE_UP:
				player.setVelocity(0, 1.5, 0);
				bypassStep = BypassStep.BEFORE_BYPASS;
				break;
		}
	}

	private boolean isInAir(ClientPlayerEntity player) {
		return bypassStep == BypassStep.MOVE_DOWN || player.doesNotCollide(0, -1, 0);
	}

	@Override
	public void onIsPlayerInWater(IsPlayerInWaterEvent event)
	{
		event.setInWater(false);
	}

	private enum BypassStep {
		BEFORE_BYPASS,
		MOVE_DOWN,
		WAIT,
		MOVE_UP
	}
}
