/*
 * Copyright (c) 2014-2021 Wurst-Imperium and contributors.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;

import net.minecraft.block.Block;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.network.Packet;
import net.minecraft.network.packet.s2c.play.BlockUpdateS2CPacket;
import net.minecraft.network.packet.s2c.play.ChunkDataS2CPacket;
import net.minecraft.network.packet.s2c.play.ChunkDeltaUpdateS2CPacket;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.EmptyChunk;
import net.wurstclient.Category;
import net.wurstclient.events.PacketInputListener;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.events.WorldRenderListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.BlockSetting;
import net.wurstclient.settings.EnumSetting;
import net.wurstclient.settings.SliderSetting;
import net.wurstclient.util.ChatUtils;
import net.wurstclient.util.ChunkSearcher;
import net.wurstclient.util.MinPriorityThreadFactory;
import net.wurstclient.util.RotationUtils;
import pogfactory.roasty.mc.client.render.BlockBoxRenderBuilder;
import pogfactory.roasty.mc.client.utility.color.ColorComponents;

import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public final class SearchHack extends Hack
        implements UpdateListener, PacketInputListener, WorldRenderListener {
    private static final ColorComponents highlightColor = new ColorComponents(.48f, .16f, .64f, .48f);

    private final BlockSetting block = new BlockSetting("Block",
            "The type of block to search for.", "minecraft:diamond_ore", false);

    private final EnumSetting<Area> area = new EnumSetting<>("Area",
            "The area around the player to search in.\n"
                    + "Higher values require a faster computer.",
            Area.values(), Area.D11);

    private final SliderSetting limit = new SliderSetting("Limit",
            "The maximum number of blocks to display.\n"
                    + "Higher values require a faster computer.",
            4, 3, 6, 1,
            v -> new DecimalFormat("##,###,###").format(Math.pow(10, v)));
    private final HashMap<Chunk, ChunkSearcher> searchers = new HashMap<>();
    private final Set<Chunk> chunksToUpdate =
            Collections.synchronizedSet(new HashSet<>());
    private final Boolean matchingBlocksLock = true;
    private int prevLimit;
    private boolean notify;
    private ExecutorService pool1;
    private ForkJoinPool pool2;
    private ForkJoinTask<HashSet<BlockPos>> getMatchingBlocksTask;
    private HashSet<BlockPos> matchingBlocks;

    public SearchHack() {
        super("Search", "Helps you to find specific blocks by\n"
                + "highlighting them in rainbow color.");
        setCategory(Category.RENDER);
        addSetting(block);
        addSetting(area);
        addSetting(limit);
    }

    @Override
    public String getRenderName() {
        return getName() + " [" + block.getBlockName().replace("minecraft:", "") + "]";
    }

    @Override
    public void onEnable() {
        prevLimit = limit.getValueI();
        notify = true;

        pool1 = MinPriorityThreadFactory.newFixedThreadPool();
        pool2 = new ForkJoinPool();

        EVENTS.add(UpdateListener.class, this);
        EVENTS.add(PacketInputListener.class, this);
        EVENTS.add(WorldRenderListener.class, this);
    }

    @Override
    public void onDisable() {
        EVENTS.remove(UpdateListener.class, this);
        EVENTS.remove(PacketInputListener.class, this);
        EVENTS.remove(WorldRenderListener.class, this);

        stopPool2Tasks();
        pool1.shutdownNow();
        pool2.shutdownNow();
        chunksToUpdate.clear();
    }

    @Override
    public void onReceivedPacket(PacketInputEvent event) {
        ClientPlayerEntity player = MC.player;
        ClientWorld world = MC.world;
        if (player == null || world == null)
            return;

        Packet<?> packet = event.getPacket();
        Chunk chunk;

        if (packet instanceof BlockUpdateS2CPacket) {
            BlockUpdateS2CPacket change = (BlockUpdateS2CPacket) packet;
            BlockPos pos = change.getPos();
            chunk = world.getChunk(pos);

        } else if (packet instanceof ChunkDeltaUpdateS2CPacket) {
            ChunkDeltaUpdateS2CPacket change =
                    (ChunkDeltaUpdateS2CPacket) packet;

            ArrayList<BlockPos> changedBlocks = new ArrayList<>();
            change.visitUpdates((pos, state) -> changedBlocks.add(pos));
            if (changedBlocks.isEmpty())
                return;

            chunk = world.getChunk(changedBlocks.get(0));

        } else if (packet instanceof ChunkDataS2CPacket) {
            ChunkDataS2CPacket chunkData = (ChunkDataS2CPacket) packet;
            chunk = world.getChunk(chunkData.getX(), chunkData.getZ());

        } else
            return;

        chunksToUpdate.add(chunk);
    }

    @Override
    public void onUpdate() {
        Block currentBlock = block.getBlock();
        BlockPos eyesPos = new BlockPos(RotationUtils.getEyesPos());

        ChunkPos center = getPlayerChunkPos(eyesPos);
        int range = area.getSelected().chunkRange;
		assert MC.world != null;
		int dimensionId = MC.world.getRegistryKey().toString().hashCode();

        addSearchersInRange(center, range, currentBlock, dimensionId);
        removeSearchersOutOfRange(center, range);
        replaceSearchersWithDifferences(currentBlock, dimensionId);
        replaceSearchersWithChunkUpdate(currentBlock, dimensionId);

        if (!areAllChunkSearchersDone())
            return;

        checkIfLimitChanged();

        if (getMatchingBlocksTask == null)
            startGetMatchingBlocksTask(eyesPos);

        if (!getMatchingBlocksTask.isDone())
            return;

        synchronized (matchingBlocksLock) {
            matchingBlocks = getMatchingBlocksFromTask();
        }
    }

    @Override
    public void onWorldRender(WorldRenderEvent event) {
        synchronized (matchingBlocksLock) {
            if (matchingBlocks == null) {
                return;
            }
            matchingBlocks.forEach(block ->
                    BlockBoxRenderBuilder.build(event)
                            .block(block)
                            .fill(highlightColor)
                            .draw()
            );
        }
    }

    private ChunkPos getPlayerChunkPos(BlockPos eyesPos) {
        int chunkX = eyesPos.getX() >> 4;
        int chunkZ = eyesPos.getZ() >> 4;
		assert MC.world != null;
		return MC.world.getChunk(chunkX, chunkZ).getPos();
    }

    private void addSearchersInRange(ChunkPos center, int chunkRange,
                                     Block block, int dimensionId) {
        ArrayList<Chunk> chunksInRange = getChunksInRange(center, chunkRange);

        for (Chunk chunk : chunksInRange) {
            if (searchers.containsKey(chunk))
                continue;

            addSearcher(chunk, block, dimensionId);
        }
    }

    private ArrayList<Chunk> getChunksInRange(ChunkPos center, int chunkRange) {
        ArrayList<Chunk> chunksInRange = new ArrayList<>();

        for (int x = center.x - chunkRange; x <= center.x + chunkRange; x++)
            for (int z = center.z - chunkRange; z <= center.z + chunkRange; z++) {
				assert MC.world != null;
				Chunk chunk = MC.world.getChunk(x, z);
                if (chunk instanceof EmptyChunk)
                    continue;

                chunksInRange.add(chunk);
            }

        return chunksInRange;
    }

    private void removeSearchersOutOfRange(ChunkPos center, int chunkRange) {
        for (ChunkSearcher searcher : new ArrayList<>(searchers.values())) {
            ChunkPos searcherPos = searcher.getChunk().getPos();

            if (Math.abs(searcherPos.x - center.x) <= chunkRange
                    && Math.abs(searcherPos.z - center.z) <= chunkRange)
                continue;

            removeSearcher(searcher);
        }
    }

    private void replaceSearchersWithDifferences(Block currentBlock,
                                                 int dimensionId) {
        for (ChunkSearcher oldSearcher : new ArrayList<>(searchers.values())) {
            if (currentBlock.equals(oldSearcher.getBlock())
                    && dimensionId == oldSearcher.getDimensionId())
                continue;

            removeSearcher(oldSearcher);
            addSearcher(oldSearcher.getChunk(), currentBlock, dimensionId);
        }
    }

    private void replaceSearchersWithChunkUpdate(Block currentBlock,
                                                 int dimensionId) {
        synchronized (chunksToUpdate) {
            if (chunksToUpdate.isEmpty())
                return;

            for (Iterator<Chunk> itr = chunksToUpdate.iterator(); itr.hasNext(); ) {
                Chunk chunk = itr.next();

                ChunkSearcher oldSearcher = searchers.get(chunk);
                if (oldSearcher == null)
                    continue;

                removeSearcher(oldSearcher);
                addSearcher(chunk, currentBlock, dimensionId);
                itr.remove();
            }
        }
    }

    private void addSearcher(Chunk chunk, Block block, int dimensionId) {
        stopPool2Tasks();

        ChunkSearcher searcher = new ChunkSearcher(chunk, block, dimensionId);
        searchers.put(chunk, searcher);
        searcher.startSearching(pool1);
    }

    private void removeSearcher(ChunkSearcher searcher) {
        stopPool2Tasks();

        searchers.remove(searcher.getChunk());
        searcher.cancelSearching();
    }

    private void stopPool2Tasks() {
        if (getMatchingBlocksTask != null) {
            getMatchingBlocksTask.cancel(true);
            getMatchingBlocksTask = null;
        }
    }

    private boolean areAllChunkSearchersDone() {
        for (ChunkSearcher searcher : searchers.values())
            if (searcher.getStatus() != ChunkSearcher.Status.DONE)
                return false;

        return true;
    }

    private void checkIfLimitChanged() {
        if (limit.getValueI() != prevLimit) {
            stopPool2Tasks();
            notify = true;
            prevLimit = limit.getValueI();
        }
    }

    private void startGetMatchingBlocksTask(BlockPos eyesPos) {
        int maxBlocks = (int) Math.pow(10, limit.getValueI());

        Callable<HashSet<BlockPos>> task =
                () -> searchers.values().parallelStream()
                        .flatMap(searcher -> searcher.getMatchingBlocks().stream())
                        .sorted(Comparator
                                .comparingInt(eyesPos::getManhattanDistance))
                        .limit(maxBlocks)
                        .collect(Collectors.toCollection(HashSet::new));

        getMatchingBlocksTask = pool2.submit(task);
    }

    private HashSet<BlockPos> getMatchingBlocksFromTask() {
        HashSet<BlockPos> matchingBlocks;

        try {
            matchingBlocks = getMatchingBlocksTask.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        int maxBlocks = (int) Math.pow(10, limit.getValueI());

        if (matchingBlocks.size() < maxBlocks)
            notify = true;
        else if (notify) {
            ChatUtils.warning("Search found \u00a7lA LOT\u00a7r of blocks!"
                    + " To prevent lag, it will only show the closest \u00a76"
                    + limit.getValueString() + "\u00a7r results.");
            notify = false;
        }

        return matchingBlocks;
    }

    private enum Area {
        D3("3x3 chunks", 1),
        D5("5x5 chunks", 2),
        D7("7x7 chunks", 3),
        D9("9x9 chunks", 4),
        D11("11x11 chunks", 5),
        D13("13x13 chunks", 6),
        D15("15x15 chunks", 7),
        D17("17x17 chunks", 8),
        D19("19x19 chunks", 9),
        D21("21x21 chunks", 10),
        D23("23x23 chunks", 11),
        D25("25x25 chunks", 12),
        D27("27x27 chunks", 13),
        D29("29x29 chunks", 14),
        D31("31x31 chunks", 15),
        D33("33x33 chunks", 16);

        private final String name;
        private final int chunkRange;

        Area(String name, int chunkRange) {
            this.name = name;
            this.chunkRange = chunkRange;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
