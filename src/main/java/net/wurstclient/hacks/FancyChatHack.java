/*
 * Copyright (c) 2014-2021 Wurst-Imperium and contributors.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;

import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.ChatOutputListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.EnumSetting;

@SearchTags({"fancy chat"})
public final class FancyChatHack extends Hack implements ChatOutputListener
{

	private static final String BLACKLIST = "(){}[]|";
	private static final String CLIENT_AD = " | RoastyWurst";
	private static final String COLOR_CODE_PREFIX = "\u00a7"; // §

	private final EnumSetting<Mode> mode = new EnumSetting<>("Mode",
			"\u00a7lOriginal\u00a7r mode sends fancy characters like you\n"
					+ "are used to in Wurst. Does not work on servers that block unicode\n"
					+ "characters.\n\n"
					+ "\u00a7lBraces\u00a7r mode puts every character in (round braces)\n\n"
					+ "\u00a7lBrackets\u00a7r mode puts every character in [brackets]\n\n"
					+ "\u00a7lCurly Braces\u00a7r mode puts every character in {curly braces}\n\n"
					+ "\u00a7lAll Caps\u00a7r mode transforms every character into UPPERCASE\n\n",
			Mode.values(), Mode.ORIGINAL);

	private final CheckboxSetting greenText = new CheckboxSetting(
			"Green text", "Green text like 4chan", false);
	private final CheckboxSetting greenTextUsingCode = new CheckboxSetting(
			"Green text (code)", "Uses a color code to turn the text green", false);

	private final CheckboxSetting advertise =
			new CheckboxSetting("Advertise client", "Send client name", false);

	private final EnumSetting<ColorMode> colorMode = new EnumSetting<>("Color", "COLOR!",
			ColorMode.values(), ColorMode.NONE);

	public FancyChatHack()
	{
		super("FancyChat", "Replaces ASCII characters in sent chat messages\n"
			+ "with fancier unicode characters or transforms them. Can be used to\n"
			+ "bypass curse word filters on some servers.\n");
		setCategory(Category.CHAT);
		addSetting(mode);
		addSetting(greenText);
		addSetting(greenTextUsingCode);
		addSetting(advertise);
		addSetting(colorMode);
	}
	
	@Override
	public void onEnable()
	{
		EVENTS.add(ChatOutputListener.class, this);
	}
	
	@Override
	public void onDisable()
	{
		EVENTS.remove(ChatOutputListener.class, this);
	}
	
	@Override
	public void onSentMessage(ChatOutputEvent event)
	{
		String message = event.getOriginalMessage();
		if(message.startsWith("/") || message.startsWith("."))
			return;

		String newMessage = convertString(message);

		if (advertise.isChecked()) {
			if (mode.getSelected() == Mode.ORIGINAL) {
				newMessage += convertStringToFancyUnicode(CLIENT_AD);
			} else {
				newMessage += CLIENT_AD;
			}
		}

		newMessage = addColor(newMessage);

		if (greenText.isChecked()) {
			newMessage = "> " + newMessage;
		}
		if (greenTextUsingCode.isChecked()) {
			newMessage = "&a" + newMessage;
		}

		event.setMessage(newMessage);
	}
	
	private String convertString(String input)
	{
		switch (mode.getSelected()) {
			case ORIGINAL:
				return convertStringToFancyUnicode(input);
			case BRACES:
				return addBracesToString(input, '(', ')');
			case BRACKETS:
				return addBracesToString(input, '[', ']');
			case CURLY_BRACES:
				return addBracesToString(input, '{', '}');
			case ALL_CAPS:
				return input.toUpperCase();
			default:
				return input;
		}
	}

	private String addBracesToString(String input, char openingBrace, char closingBrace) {
		StringBuilder outputBuilder = new StringBuilder(input.length() * 3);
		for (char c : input.toCharArray()) {
			if (c == ' ') {
				outputBuilder.append(c);
			} else {
				outputBuilder.append(openingBrace).append(c).append(closingBrace);
			}
		}
		return outputBuilder.toString();
	}

	private String convertStringToFancyUnicode(String input) {
		StringBuilder output = new StringBuilder();
		for (char c : input.toCharArray())
			output.append(convertChar(c));

		return output.toString();
	}

	private String convertChar(char c)
	{
		if(c < 0x21 || c > 0x80)
			return "" + c;

		if(BLACKLIST.contains(Character.toString(c)))
			return "" + c;
		
		return new String(Character.toChars(c + 0xfee0));
	}

	private String addColor(String input) {
		return colorMode.getSelected().getColorCode() + input;
	}

	private enum Mode
	{
		NONE("None"),
		ORIGINAL("Original"),
		BRACES("Braces"),
		BRACKETS("Brackets"),
		CURLY_BRACES("Curly Braces"),
		ALL_CAPS("All Caps");

		private final String name;

		Mode(String name)
		{
			this.name = name;
		}

		@Override
		public String toString()
		{
			return name;
		}
	}

	private enum ColorMode
	{
		NONE("None", ""),
		BLACK("Black", "0"),
		DARK_BLUE("Dark Blue", "1"),
		DARK_GREEN("Dark Green", "2"),
		DARK_AQUA("Dark Aqua", "3"),
		DARK_RED("Dark Red", "4"),
		DARK_PURPLE("Dark Purple", "5"),
		GOLD("Gold", "6"),
		GRAY("Gray", "7"),
		DARK_GRAY("Dark Gray", "8"),
		BLUE("Blue", "9"),
		GREEN("Green", "a"),
		AQUA("Aqua", "b"),
		RED("Red", "c"),
		LIGHT_PURPLE("Light Purple", "d"),
		YELLOW("Yellow", "e"),
		WHITE("White", "f");

		private final String name;
		private final String colorCode;

		ColorMode(String name, String colorCode)
		{
			this.name = name;
			this.colorCode = colorCode;
		}

		@Override
		public String toString()
		{
			return getColorCode() + name;
		}

		public String getColorCode() {
			if (this == NONE) return "";
			return COLOR_CODE_PREFIX + colorCode;
		}
	}
}
