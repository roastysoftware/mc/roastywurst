/*
 * This file is part of the BleachHack distribution (https://github.com/BleachDrinker420/bleachhack-1.14/).
 * Copyright (c) 2019 Bleach.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wurstclient.hacks;

import bleach.hack.utils.WorldUtils;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.HorseBaseEntity;
import net.minecraft.entity.passive.LlamaEntity;
import net.minecraft.entity.vehicle.MinecartEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.Category;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.SliderSetting;

public class EntityControlHack extends Hack implements UpdateListener {

    private static final CheckboxSetting entitySpeed = new CheckboxSetting(
            "EntitySpeed", true);
    private static final SliderSetting speedSlider = new SliderSetting(
            "Speed", 1.2, 0, 5, 0.2, SliderSetting.ValueDisplay.DECIMAL);
    private static final CheckboxSetting entityFly = new CheckboxSetting(
            "EntityFly", false);
    private static final SliderSetting ascend = new SliderSetting(
            "Ascend", 0.3, 0, 2, 0.1, SliderSetting.ValueDisplay.DECIMAL);
    private static final SliderSetting descend = new SliderSetting(
            "Descend", 0.5, 0, 2, 0.1, SliderSetting.ValueDisplay.DECIMAL);
    private static final CheckboxSetting groundSnap = new CheckboxSetting(
            "Ground Snap", false);
    private static final CheckboxSetting antiStuck = new CheckboxSetting(
            "AntiStuck", false);


    public EntityControlHack() {
        super("EntityControl", "Manipulate Entities.");
        setCategory(Category.MOVEMENT);
        addSetting(entitySpeed);
        addSetting(speedSlider);
        addSetting(entityFly);
        addSetting(ascend);
        addSetting(descend);
        addSetting(groundSnap);
        addSetting(antiStuck);
    }

    @Override
    protected void onEnable() {
        EVENTS.add(UpdateListener.class, this);
    }

    @Override
    protected void onDisable() {
        EVENTS.remove(UpdateListener.class, this);
    }

    @Override
    public void onUpdate() {
        if (MC.player.getVehicle() == null) return;

        Entity e = MC.player.getVehicle();
        e.yaw = MC.player.yaw;
        double speed = speedSlider.getValue();

        if (antiStuck.isChecked() && e instanceof HorseBaseEntity) {
            HorseBaseEntity h = (HorseBaseEntity) e;
            h.saddle(null);
            h.setTame(true);
            h.setAiDisabled(true);
        }

        if (e instanceof LlamaEntity) {
            ((LlamaEntity) e).headYaw = MC.player.headYaw;
        }

        double forward = MC.player.forwardSpeed;
        double strafe = MC.player.sidewaysSpeed;
        float yaw = MC.player.yaw;

        if (entitySpeed.isChecked()) {
            if ((forward == 0.0D) && (strafe == 0.0D)) {
                e.setVelocity(0, e.getVelocity().y, 0);
            } else {
                if (forward != 0.0D) {
                    if (strafe > 0.0D) {
                        yaw += (forward > 0.0D ? -45 : 45);
                    } else if (strafe < 0.0D) yaw += (forward > 0.0D ? 45 : -45);
                    strafe = 0.0D;
                    if (forward > 0.0D) {
                        forward = 1.0D;
                    } else if (forward < 0.0D) forward = -1.0D;
                }
                double cos = Math.cos(Math.toRadians(yaw + 90.0F));
                double sin = Math.sin(Math.toRadians(yaw + 90.0F));
                double velocityX = forward * speed * cos + strafe * speed * sin;
                double velocityZ = forward * speed * sin - strafe * speed * cos;
                e.setVelocity(velocityX, e.getVelocity().y,
                        velocityZ);
                if (e instanceof MinecartEntity) {
                    MinecartEntity em = (MinecartEntity) e;
                    em.setVelocity(velocityX, em.getVelocity().y, velocityZ);
                }
            }
        }

        if (entityFly.isChecked()) {
            if (MC.options.keyJump.isPressed()) {
                e.setVelocity(e.getVelocity().x, ascend.getValue(), e.getVelocity().z);
            } else {
                e.setVelocity(e.getVelocity().x, -descend.getValue(), e.getVelocity().z);
            }
        }

        if (groundSnap.isChecked()) {
            BlockPos p = new BlockPos(e.getPos());
            if (!WorldUtils.NONSOLID_BLOCKS.contains(MC.world.getBlockState(p.down()).getBlock()) && e.fallDistance > 0.01) {
                e.setVelocity(e.getVelocity().x, -1, e.getVelocity().z);
            }
        }

        if (antiStuck.isChecked()) {
            Vec3d vel = e.getVelocity().multiply(2);
            if (!WorldUtils.isBoxEmpty(WorldUtils.moveBox(e.getBoundingBox(), vel.x, 0, vel.z))) {
                for (int i = 2; i < 10; i++) {
                    if (WorldUtils.isBoxEmpty(WorldUtils.moveBox(e.getBoundingBox(), vel.x / i, 0, vel.z / i))) {
                        e.setVelocity(vel.x / i / 2, vel.y, vel.z / i / 2);
                        break;
                    }
                }
            }
        }
    }
}
