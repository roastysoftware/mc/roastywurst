/*
 * Parts of this file are licensed as follows:
 * Copyright (C) 2014 - 2020 | Alexander01998 | All rights reserved.
 * Copyright (c) 2019 Bleach.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.hacks;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.packet.c2s.play.ClientCommandC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayerMoveC2SPacket;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.*;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.EnumSetting;
import net.wurstclient.settings.SliderSetting;
import net.wurstclient.util.ChatUtils;

@SearchTags({"EasyElytra", "ElytraFly", "elytra hack"})
public final class ElytraFlyHack extends Hack
        implements UpdateListener, ClientMoveListener, CameraGetFieldListener,
        PreWorldRenderListener, PostWorldRenderListener {

    private static final float UPLIFT_PITCH = -42.069f;

    public final SliderSetting speed =
            new SliderSetting("Speed", 0.8, 0, 5, 0.05, SliderSetting.ValueDisplay.DECIMAL);

    private final EnumSetting<Mode> mode = new EnumSetting<>("Mode",
            "\u00a7lNormal\u00a7r mode is just... normal mode\n\n"
                    + "\u00a7lControl\u00a7r mode let's you fly like FitMC\n\n"
                    + "\u00a7lBruh Momentum\u00a7r is for insane people\n\n",
            Mode.values(), Mode.NORMAL);

    private final CheckboxSetting vanillaUplift = new CheckboxSetting(
            "Vanilla Uplift (VU)", "Use Vanilla mechanics to fly upwards (bypasses anticheat)", false);


    private final CheckboxSetting vuSpacebar = new CheckboxSetting(
            "[VU] Use spacebar",
            "Use spacebar to go up instead of you looking into the sky " +
                    "(might cause glitches but allows you to look down while going up)", false);

    private boolean shouldChangeCameraPitch = false;
    private float originalPitch;
    private long lastTooLowPitchMessage;

    private Vec3d lastPosition;
    private Vec3d pos;

    public ElytraFlyHack() {
        super("ElytraFly", "Lets you hack around with your Elytra.");
        setCategory(Category.MOVEMENT);
        addSetting(speed);
        addSetting(mode);
        addSetting(vanillaUplift);
        addSetting(vuSpacebar);
    }

    @Override
    public void onEnable() {
        EVENTS.add(UpdateListener.class, this);
        EVENTS.add(ClientMoveListener.class, this);
        EVENTS.add(CameraGetFieldListener.class, this);
        EVENTS.add(PreWorldRenderListener.class, this);
        EVENTS.add(PostWorldRenderListener.class, this);
    }

    @Override
    public void onDisable() {
        EVENTS.remove(UpdateListener.class, this);
        EVENTS.remove(ClientMoveListener.class, this);
        EVENTS.remove(CameraGetFieldListener.class, this);
        EVENTS.remove(PreWorldRenderListener.class, this);
        EVENTS.remove(PostWorldRenderListener.class, this);
        lastPosition = null;
    }

    @Override
    public void onUpdate() {

        ItemStack chest = MC.player.getEquippedStack(EquipmentSlot.CHEST);
        if (chest.getItem() != Items.ELYTRA)
            return;

        float speedValue = speed.getValueF();

        Vec3d vec3d = new Vec3d(0, 0, speedValue)
                .rotateX(mode.getSelected() == Mode.CONTROL ? 0 : -(float) Math.toRadians(MC.player.pitch))
                .rotateY(-(float) Math.toRadians(MC.player.yaw));

        if (vuSpacebar.isChecked()) {
            if (MC.player.pitch != UPLIFT_PITCH) {
                originalPitch = MC.player.pitch;
            } else if (MC.player.prevPitch != UPLIFT_PITCH) {
                originalPitch = MC.player.prevPitch;
            }
        }

        if (MC.player.isFallFlying()) {
            if (mode.getSelected() == Mode.NORMAL) {
                MC.player.setVelocity(
                        MC.player.getVelocity().x + vec3d.x + (vec3d.x - MC.player.getVelocity().x),
                        MC.player.getVelocity().y + vec3d.y + (vec3d.y - MC.player.getVelocity().y),
                        MC.player.getVelocity().z + vec3d.z + (vec3d.z - MC.player.getVelocity().z));
            } else if (mode.getSelected() == Mode.CONTROL) {
                if (MC.options.keyBack.isPressed()) vec3d = vec3d.multiply(-1);
                if (MC.options.keyLeft.isPressed()) vec3d = vec3d.rotateY((float) Math.toRadians(90));
                if (MC.options.keyRight.isPressed()) vec3d = vec3d.rotateY(-(float) Math.toRadians(90));

                if (vanillaUplift.isChecked()) {
                    lastPosition = pos;
                    pos = MC.player.getPos();
                    if (MC.options.keyJump.isPressed()) {
                        if (vuSpacebar.isChecked()) {
                            MC.player.networkHandler.sendPacket(
                                    new PlayerMoveC2SPacket.LookOnly(MC.player.yaw, UPLIFT_PITCH, false));
                            MC.player.pitch = UPLIFT_PITCH;
                            return;
                        } else {
                            vec3d = vec3d.add(0, speedValue, 0);
                        }
                    }
                    if (shouldVanillaUpliftWithoutSpacebar()) {
                        return;
                    }
                }
                if (MC.options.keySneak.isPressed()) {
                    // To prevent damage
                    if (MC.player.pitch > 30) {
                        MC.player.pitch = 30;
                        if (lastTooLowPitchMessage + 2000 <= System.currentTimeMillis()) {
                            ChatUtils.warning("Your pitch is clamped to prevent damage! " +
                                    "You can't look further down than that without taking damage.");
                            lastTooLowPitchMessage = System.currentTimeMillis();
                        }
                    }
                    return;
                }
                if (!MC.options.keyBack.isPressed() && !MC.options.keyLeft.isPressed()
                        && !MC.options.keyRight.isPressed() && !MC.options.keyForward.isPressed()
                        && !MC.options.keyJump.isPressed() && !MC.options.keySneak.isPressed()) vec3d = Vec3d.ZERO;
                MC.player.setVelocity(vec3d.multiply(2));
            }
        } else if (mode.getSelected() == Mode.BRUH_MOMENTUM && !MC.player.isOnGround()
                && MC.player.fallDistance > 0.5) {
            /* I tried packet mode and got whatever the fuck **i mean frick** this is */
            if (MC.options.keySneak.isPressed()) return;
            MC.player.setVelocity(vec3d);
            MC.player.networkHandler.sendPacket(new ClientCommandC2SPacket(MC.player, ClientCommandC2SPacket.Mode.START_FALL_FLYING));
            MC.player.networkHandler.sendPacket(new PlayerMoveC2SPacket(true));
        } else {
            lastPosition = null;
        }
    }

    private boolean hasLostMomentum() {
        return lastPosition != null && pos != null && lastPosition.distanceTo(pos) < 0.3 && !MC.options.keySneak.isPressed();
    }

    @Override
    public void onClientMove(ClientMoveEvent event) {
        /* Cancel the retarded auto elytra movement */
        if (mode.getSelected() == Mode.CONTROL && MC.player.isFallFlying()) {
            if (shouldVanillaUpliftUsingSpacebar() || shouldVanillaUpliftWithoutSpacebar()) {
                return;
            }
            if (MC.options.keySneak.isPressed()) {
                return;
            }
            if (!MC.options.keyJump.isPressed() && !MC.options.keySneak.isPressed()) {
                event.vec3d = new Vec3d(event.vec3d.x, 0, event.vec3d.z);
            }

            if (!MC.options.keyBack.isPressed() && !MC.options.keyLeft.isPressed()
                    && !MC.options.keyRight.isPressed() && !MC.options.keyForward.isPressed()) {
                event.vec3d = new Vec3d(0, event.vec3d.y, 0);
            }
        }
    }

    private boolean shouldVanillaUpliftUsingSpacebar() {
        return MC.player.isFallFlying() && vanillaUplift.isChecked() && vuSpacebar.isChecked() && MC.options.keyJump.isPressed() &&
                !hasLostMomentum();
    }

    private boolean shouldVanillaUpliftWithoutSpacebar() {
        // fallDistance is 1 when flying
        return MC.player.isFallFlying() && !vuSpacebar.isChecked() && MC.player.pitch < 0 && MC.player.fallDistance == 1 &&
                !hasLostMomentum();
    }

    @Override
    public void onGetPitch(CameraGetFieldEvent event) {
        if (shouldChangeCameraPitch && shouldVanillaUpliftUsingSpacebar()) {
            MC.player.pitch = originalPitch;
            event.setPitch(originalPitch);
        }
    }

    @Override
    public void onPostWorldRender() {
        shouldChangeCameraPitch = false;
    }

    @Override
    public void onPreWorldRender() {
        shouldChangeCameraPitch = true;
    }

    private enum Mode {
        NORMAL("Normal"),
        CONTROL("Control"),
        BRUH_MOMENTUM("Bruh Momentum");

        private final String name;

        private Mode(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

}
