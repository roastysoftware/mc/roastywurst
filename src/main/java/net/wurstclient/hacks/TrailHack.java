/*
 * This file is part of the BleachHack distribution (https://github.com/BleachDrinker420/bleachhack-1.14/).
 * Copyright (c) 2019 Bleach.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wurstclient.hacks;

import bleach.hack.utils.RenderUtils;
import com.google.common.collect.Iterables;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.Category;
import net.wurstclient.events.RenderListener;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.EnumSetting;
import net.wurstclient.settings.SliderSetting;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrailHack extends Hack implements RenderListener, UpdateListener {

    private final CheckboxSetting trail = new CheckboxSetting("Trail", true);
    private final CheckboxSetting keepTrail = new CheckboxSetting("Keep Trail", false);
    private final EnumSetting<ColorMode> colorMode = new EnumSetting<>("Color", ColorMode.values(), ColorMode.RED);
    private final SliderSetting thickness = new SliderSetting(
            "Thickness", 3, 0.1, 10, 1, SliderSetting.ValueDisplay.DECIMAL);

    private List<List<Vec3d>> trails = new ArrayList<>();

    public TrailHack() {
        super("Trail", "Shows a trail where you go");
        setCategory(Category.RENDER);
        addSetting(trail);
        addSetting(keepTrail);
        addSetting(colorMode);
        addSetting(thickness);
    }

    @Override
    public void onEnable() {
        EVENTS.add(UpdateListener.class, this);
        EVENTS.add(RenderListener.class, this);
        if (!keepTrail.isChecked()) trails.clear();
    }

    @Override
    protected void onDisable() {
        EVENTS.remove(RenderListener.class, this);
        EVENTS.remove(UpdateListener.class, this);
    }

    public void onUpdate() {
        if (!trail.isChecked()) return;

        if (trails.isEmpty()) trails.add(Arrays.asList(MC.player.getPos().add(0, 0.1, 0), MC.player.getPos()));
        else if (MC.player.getPos().add(0, 0.1, 0).distanceTo(Iterables.getLast(trails).get(1)) > 0.15) {
            trails.add(Arrays.asList(Iterables.getLast(trails).get(1), MC.player.getPos().add(0, 0.1, 0)));
        }
    }

    public void onRender(float partialTicks) {
        Color clr = Color.BLACK;
        if (colorMode.getSelected() == ColorMode.RED) clr = new Color(200, 50, 50);
        else if (colorMode.getSelected() == ColorMode.GREEN) clr = new Color(50, 200, 50);
        else if (colorMode.getSelected() == ColorMode.BLUE) clr = new Color(50, 50, 200);

        int count = 250;
        boolean rev = false;
        for (List<Vec3d> e : trails) {
            if (colorMode.getSelected() == ColorMode.B2G) clr = new Color(50, 255 - count, count);
            else if (colorMode.getSelected() == ColorMode.R2B) clr = new Color(count, 50, 255 - count);
            RenderUtils.drawLine(e.get(0).x, e.get(0).y, e.get(0).z, e.get(1).x, e.get(1).y, e.get(1).z,
                    clr.getRed() / 255f, clr.getGreen() / 255f, clr.getBlue() / 255f,
                    thickness.getValueF());
            if (count < 5 || count > 250) rev = !rev;
            count += rev ? 3 : -3;
        }
    }

    private enum ColorMode {
        RED("Red"),
        GREEN("Green"),
        BLUE("Blue"),
        B2G("B2G"),
        R2B("R2B");

        private final String name;

        ColorMode(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }


    }

}
