package net.wurstclient.hacks;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.screen.slot.SlotActionType;
import net.wurstclient.Category;
import net.wurstclient.hack.Hack;
import net.wurstclient.util.ItemUtils;

public class ElytraSwapHack extends Hack {

    public ElytraSwapHack() {
        super("ElytraSwap", "Swaps the Elytra with a Chestplate in your inventory");
        setCategory(Category.MOVEMENT);
    }

    private void swap() {
        boolean isElytraEquipped = MC.player.inventory.getArmorStack(2).getItem() == Items.ELYTRA;
        for (int slot = 0; slot < 44; slot++) {
            boolean foundItem;
            Item item = MC.player.inventory.getStack(slot).getItem();
            if (isElytraEquipped) {
                foundItem = ItemUtils.isChestplate(item);
            } else {
                foundItem = item == Items.ELYTRA;
            }
            if (foundItem) {
                MC.interactionManager.clickSlot(0, slot < 9 ? slot + 36 : slot, 0, SlotActionType.PICKUP, MC.player);
                MC.interactionManager.clickSlot(0, 6, 0, SlotActionType.PICKUP, MC.player);
                MC.interactionManager.clickSlot(0, slot < 9 ? slot + 36 : slot, 0, SlotActionType.PICKUP, MC.player);
            }
        }
    }

    @Override
    public void onEnable() {
        swap();
        setEnabled(false);
    }

}
