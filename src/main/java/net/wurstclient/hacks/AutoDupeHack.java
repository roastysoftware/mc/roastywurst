// Partially inspired by BleachHack
package net.wurstclient.hacks;

import net.minecraft.block.ShulkerBoxBlock;
import net.minecraft.client.util.InputUtil;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.AbstractDonkeyEntity;
import net.minecraft.entity.passive.LlamaEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.wurstclient.Category;
import net.wurstclient.SearchTags;
import net.wurstclient.events.OpenContainerScreenListener;
import net.wurstclient.events.UpdateListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.EnumSetting;
import net.wurstclient.util.ItemUtils;
import org.lwjgl.glfw.GLFW;

@SearchTags({"Dupe", "Donkey"})
public class AutoDupeHack extends Hack implements UpdateListener {

    private final EnumSetting<Mode> mode = new EnumSetting<>(
            "Method", Mode.values(), Mode.DONKEY_116);

    private boolean isPutTick = false;
    private int donkeySlotCount;
    private AbstractDonkeyEntity lastDonkey;

    public AutoDupeHack() {
        super("AutoDupe", "Dupes stuff");
        setCategory(Category.ITEMS);
        addSetting(mode);
    }

    @Override
    protected void onEnable() {
        EVENTS.add(UpdateListener.class, this);
    }

    @Override
    protected void onDisable() {
        EVENTS.remove(UpdateListener.class, this);
    }

    @Override
    public void onUpdate() {
        AbstractDonkeyEntity donkey;
        if (!MC.player.hasVehicle()) {
            donkey = lastDonkey;
        } else {
            Entity entity = MC.player.getVehicle();
            if (!(entity instanceof AbstractDonkeyEntity)) {
                return;
            }

            donkey = (AbstractDonkeyEntity) entity;
        }

        if (donkey != lastDonkey) {
            lastDonkey = donkey;
        }

        donkeySlotCount = getInvSize(donkey);

        if (!canDupeNow(donkey)) {
            return;
        }

        for (int i = 2; i < donkeySlotCount + MC.player.inventory.size() - 3; i++) {
            Slot slot = MC.player.currentScreenHandler.slots.get(i);
            ItemStack itemStack = slot.getStack();
            if (ItemUtils.isShulkerBox(itemStack.getItem())) {
                // Move shulker
                if (isPutTick && i < donkeySlotCount || !isPutTick && i >= donkeySlotCount) {
                    MC.interactionManager.clickSlot(
                            MC.player.currentScreenHandler.syncId, slot.id, 0, SlotActionType.QUICK_MOVE, MC.player);
                }
            }
        }
        // Next step: opposite
        isPutTick = !isPutTick;

    }

    private int findEmptyDonkeySlot(AbstractDonkeyEntity e) {
        for (int i = 2; i <= getDupeSize(e); i++) {
            if (!MC.player.currentScreenHandler.slots.get(i).hasStack()) {
                return i;
            }
        }
        return -1;
    }

    private int getInvSize(Entity e) {
        if (!(e instanceof AbstractDonkeyEntity)) return -1;
        if (!((AbstractDonkeyEntity) e).hasChest()) return 0;

        if (e instanceof LlamaEntity) {
            return 3 * ((LlamaEntity) e).getStrength();
        }

        return 15;
    }

    private boolean isInCorrectInventory(AbstractDonkeyEntity e) {
        return e != null && e.hasChest() && MC.player.currentScreenHandler.slots.size() != 46;
    }

    private boolean canDupeNow(AbstractDonkeyEntity e) {
        return isInCorrectInventory(e);
    }

    private int getDupeSize(AbstractDonkeyEntity e) {
        if (!isInCorrectInventory(e)) {
            return 0;
        }

        return MC.player.currentScreenHandler.slots.size() - 38;
    }


    private enum Mode {
        DONKEY_116("Donkey 1.16");

        private final String name;

        Mode(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

}
