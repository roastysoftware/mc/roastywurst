/*
 * This file is part of the BleachHack distribution (https://github.com/BleachDrinker420/bleachhack-1.14/).
 * Copyright (c) 2019 Bleach.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wurstclient.hacks;

import bleach.hack.utils.FabricReflect;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.s2c.play.WorldTimeUpdateS2CPacket;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.Category;
import net.wurstclient.events.GUIRenderListener;
import net.wurstclient.events.PacketInputListener;
import net.wurstclient.hack.Hack;
import net.wurstclient.settings.CheckboxSetting;
import net.wurstclient.settings.EnumSetting;
import org.lwjgl.opengl.GL11;

import java.util.*;
import java.util.stream.Collectors;

public class UIHack extends Hack implements GUIRenderListener, PacketInputListener {

    private static final CheckboxSetting extraLine = new CheckboxSetting("Extra Line", false); // 1
    private static final CheckboxSetting fps = new CheckboxSetting("FPS", true); // 3
    private static final CheckboxSetting ping = new CheckboxSetting("Ping", true); // 4
    private static final CheckboxSetting coords = new CheckboxSetting("Coords", true); // 5
    private static final CheckboxSetting showTps = new CheckboxSetting("TPS", true); // 6
    private static final CheckboxSetting lagMeter = new CheckboxSetting("Lag-Meter", true); // 7
    private static final CheckboxSetting server = new CheckboxSetting("Server", false); // 8
    private static final CheckboxSetting players = new CheckboxSetting("Players", false); // 9
    private static final CheckboxSetting armor = new CheckboxSetting("Armor", true); // 10
    private static final CheckboxSetting timeStamp = new CheckboxSetting("Time", false); // 11
    private static final EnumSetting<InfoPos> infoPos = new EnumSetting<>(
            "Info position", InfoPos.values(), InfoPos.TOP_RIGHT); // 15

    public List<String> infoList = new ArrayList<>();
    private int count = 0;
    private long prevTime = 0;
    private double tps = 20;
    private long lastPacket = 0;

    public UIHack() {
        super("UI", "Shows stuff onscreen.");
        setCategory(Category.RENDER);
        addSetting(extraLine);
        addSetting(fps);
        addSetting(ping);
        addSetting(coords);
        addSetting(showTps);
        addSetting(lagMeter);
        addSetting(server);
        addSetting(players);
        addSetting(armor);
        addSetting(timeStamp);
        addSetting(infoPos);
    }

    @Override
    protected void onEnable() {
        EVENTS.add(GUIRenderListener.class, this);
        EVENTS.add(PacketInputListener.class, this);
    }

    @Override
    protected void onDisable() {
        EVENTS.remove(GUIRenderListener.class, this);
        EVENTS.remove(PacketInputListener.class, this);
    }

    @Override
    public void onRenderGUI(MatrixStack matrixStack, float partialTicks) {
        infoList.clear();

        if (timeStamp.isChecked()) {
            infoList.add("\u00a77Time: \u00a7e" + Calendar.getInstance(TimeZone.getDefault()).getTime().toString());
        }

        if (coords.isChecked()) {
            boolean nether = MC.world.getRegistryKey().getValue().getPath().contains("nether");
            BlockPos pos = MC.player.getBlockPos();
            Vec3d vec = MC.player.getPos();
            BlockPos pos2 = nether ? new BlockPos(vec.getX() * 8, vec.getY(), vec.getZ() * 8)
                    : new BlockPos(vec.getX() / 8, vec.getY(), vec.getZ() / 8);

            infoList.add("XYZ: " + (nether ? "\u00a74" : "\u00a7b") + pos.getX() + " " + pos.getY() + " " + pos.getZ()
                    + " \u00a77[" + (nether ? "\u00a7b" : "\u00a74") + pos2.getX() + " " + pos2.getY() + " " + pos2.getZ() + "\u00a77]");
        }

        if (fps.isChecked()) {
            int fps = (int) FabricReflect.getFieldValue(MinecraftClient.getInstance(), "field_1738", "currentFps");
            infoList.add("FPS: " + getColorString(fps, 120, 60, 30, 15, 10, false) + fps);
        }

        if (ping.isChecked()) {
            int ping = 0;
            try {
                ping = MC.getNetworkHandler().getPlayerListEntry(MC.player.getGameProfile().getId()).getLatency();
            } catch (Exception e) {
                // nope
            }
            infoList.add("Ping: " + getColorString(ping, 75, 180, 300, 500, 1000, true) + ping);
        }

        long currentTimeMillis = System.currentTimeMillis();
        if (showTps.isChecked()) {
            String suffix = "\u00a77";
            if (lastPacket + 7500 < currentTimeMillis) suffix += "....";
            else if (lastPacket + 5000 < currentTimeMillis) suffix += "...";
            else if (lastPacket + 2500 < currentTimeMillis) suffix += "..";
            else if (lastPacket + 1200 < currentTimeMillis) suffix += ".";

            infoList.add("TPS: " + getColorString((int) tps, 18, 15, 12, 8, 4, false) + tps + suffix);
        }

        if (lagMeter.isChecked()) {
            if (currentTimeMillis - lastPacket > 500) {
                String text = "Server lagging for: " + ((currentTimeMillis - lastPacket) / 1000d) + "s";
                MC.textRenderer.drawWithShadow(matrixStack, text,
                        (float) MC.getWindow().getScaledWidth() / 2 - (float) MC.textRenderer.getWidth(text) / 2,
                        Math.min((currentTimeMillis - lastPacket - 500) / 20 - 20, 10), 0xd0d0d0);
            }
        }

        if (server.isChecked()) {
            String server = "";
            try {
                server = MC.getCurrentServerEntry().address;
            } catch (Exception e) {
                // nope
            }
            DrawableHelper.fill(matrixStack, MC.getWindow().getScaledWidth() - MC.textRenderer.getWidth(server) - 4, 2, MC.getWindow().getScaledWidth() - 3, 12, 0xa0000000);
            MC.textRenderer.drawWithShadow(matrixStack, server, MC.getWindow().getScaledWidth() - MC.textRenderer.getWidth(server) - 3, 3, 0xb0b0b0);
        }

        if (players.isChecked() && !MC.options.debugEnabled) {
            DrawableHelper.fill(matrixStack, 0, 3 + (count * 10), MC.textRenderer.getWidth("Players:") + 4, 13 + (count * 10), 0x40000000);
            MC.textRenderer.drawWithShadow(matrixStack, "Players:", 2, 4 + count * 10, 0xff0000);
            count++;

            for (Entity e : MC.world.getPlayers().stream().sorted(
                    Comparator.comparingDouble(a -> MC.player.getPos().distanceTo(a.getPos())))
                    .collect(Collectors.toList())) {
                if (e == MC.player) continue;

                String text = e.getDisplayName().getString() + " | " +
                        e.getBlockPos().getX() + " " + e.getBlockPos().getY() + " " + e.getBlockPos().getZ()
                        + " (" + Math.round(MC.player.getPos().distanceTo(e.getPos())) + "m)";

                DrawableHelper.fill(matrixStack, 0, 3 + (count * 10), MC.textRenderer.getWidth(text) + 4, 13 + (count * 10), 0x40000000);
                MC.textRenderer.drawWithShadow(matrixStack, text, 2, 4 + count * 10,
                        0xf00000 + (int) Math.min(MC.player.getPos().distanceTo(e.getPos()) * 3, 255));
                count++;
            }
        }

        if (armor.isChecked() && !MC.player.isCreative() && !MC.player.isSpectator()) {
            GL11.glPushMatrix();
            GL11.glEnable(GL11.GL_TEXTURE_2D);

            int count = 0;
            int x1 = MC.getWindow().getScaledWidth() / 2;
            int y = MC.getWindow().getScaledHeight() -
                    (MC.player.isSubmergedInWater() || MC.player.getAir() < MC.player.getMaxAir() ? 64 : 55);
            for (ItemStack is : MC.player.inventory.armor) {
                count++;
                if (is.isEmpty()) continue;
                int x = x1 - 90 + (9 - count) * 20 + 2;

                GL11.glEnable(GL11.GL_DEPTH_TEST);
                MC.getItemRenderer().zOffset = 200F;
                MC.getItemRenderer().renderGuiItemIcon(is, x, y);
                MC.getItemRenderer().renderGuiItemOverlay(MC.textRenderer, is, x, y);
                MC.getItemRenderer().zOffset = 0F;
                GL11.glEnable(GL11.GL_TEXTURE_2D);
                GL11.glDisable(GL11.GL_DEPTH_TEST);
            }
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glPopMatrix();
        }


        int count2 = 0;
        InfoPos infoPosition = infoPos.getSelected();
        for (String s : infoList) {
            MC.textRenderer.drawWithShadow(matrixStack, s,
                    infoPosition == InfoPos.DOWN_LEFT ? 2 : MC.getWindow().getScaledWidth() - MC.textRenderer.getWidth(s) - 2,
                    infoPosition == InfoPos.TOP_RIGHT ? 2 + (count2 * 10) : MC.getWindow().getScaledHeight() - 9 - (count2 * 10), 0xa0a0a0);
            count2++;
        }
    }

    public String getColorString(int value, int best, int good, int mid, int bad, int worst, boolean rev) {
        if (!rev ? value > best : value < best) return "\u00a72";
        else if (!rev ? value > good : value < good) return "\u00a7a";
        else if (!rev ? value > mid : value < mid) return "\u00a7e";
        else if (!rev ? value > bad : value < bad) return "\u00a76";
        else if (!rev ? value > worst : value < worst) return "\u00a7c";
        else return "\u00a74";
    }

    @Override
    public void onReceivedPacket(PacketInputEvent event) {
        lastPacket = System.currentTimeMillis();
        if (event.getPacket() instanceof WorldTimeUpdateS2CPacket) {
            long time = lastPacket;
            if (time < 500) return;
            long timeOffset = Math.abs(1000 - (time - prevTime)) + 1000;
            tps = Math.round(MathHelper.clamp(20 / ((double) timeOffset / 1000), 0, 20) * 100d) / 100d;
            prevTime = time;
        }
    }

    private enum InfoPos {
        DOWN_LEFT("Down Left"),
        TOP_RIGHT("Top Right"),
        BRACES("Down Right");

        private final String name;

        InfoPos(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

}
