package net.wurstclient.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.*;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.LivingEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Matrix4f;
import net.wurstclient.WurstClient;
import net.wurstclient.events.WorldRenderListener;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(WorldRenderer.class)

public class WorldRendererMixin {

    private static final MinecraftClient MC = MinecraftClient.getInstance();

    private VertexConsumerProvider.Immediate vertexConsumerProvider =
            VertexConsumerProvider.immediate(new BufferBuilder(256));;

    @Inject(at = @At(value = "RETURN"),
            method = "render(Lnet/minecraft/client/util/math/MatrixStack;FJZLnet/minecraft/client/render/Camera;Lnet/minecraft/client/render/GameRenderer;Lnet/minecraft/client/render/LightmapTextureManager;Lnet/minecraft/util/math/Matrix4f;)V")
    private void onRender(
            MatrixStack matrixStack, float tickDelta, long limitTime, boolean renderBlockOutline,
            Camera camera, GameRenderer gameRenderer, LightmapTextureManager lightmapTextureManager,
            Matrix4f matrix4f, CallbackInfo ci) {
        matrixStack.push();
        WurstClient.INSTANCE.getEventManager().fire(new WorldRenderListener.WorldRenderEvent(
                matrixStack, tickDelta, limitTime, renderBlockOutline, camera, gameRenderer,
                lightmapTextureManager, matrix4f, vertexConsumerProvider));
        matrixStack.pop();
        vertexConsumerProvider.draw();
    }

}
