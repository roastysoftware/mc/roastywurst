package net.wurstclient.util;

import net.minecraft.item.Item;
import net.minecraft.item.Items;

import java.util.Arrays;
import java.util.List;

public class ItemUtils {

    public static final List<Item> CHESTPLATES = Arrays.asList(
            Items.LEATHER_CHESTPLATE, Items.CHAINMAIL_CHESTPLATE, Items.IRON_CHESTPLATE,
            Items.DIAMOND_CHESTPLATE, Items.GOLDEN_CHESTPLATE, Items.NETHERITE_CHESTPLATE
    );

    public static final List<Item> SHULKER_BOXES = Arrays.asList(
            Items.SHULKER_BOX, Items.BLACK_SHULKER_BOX, Items.BLUE_SHULKER_BOX,
            Items.BROWN_SHULKER_BOX, Items.CYAN_SHULKER_BOX, Items.GRAY_SHULKER_BOX,
            Items.GREEN_SHULKER_BOX, Items.LIGHT_BLUE_SHULKER_BOX, Items.LIGHT_GRAY_SHULKER_BOX,
            Items.LIME_SHULKER_BOX, Items.MAGENTA_SHULKER_BOX, Items.ORANGE_SHULKER_BOX,
            Items.PINK_SHULKER_BOX, Items.RED_SHULKER_BOX, Items.WHITE_SHULKER_BOX,
            Items.YELLOW_SHULKER_BOX, Items.PURPLE_SHULKER_BOX
    );

    public static boolean isChestplate(Item item) {
        return CHESTPLATES.contains(item);
    }

    public static boolean isShulkerBox(Item item) {
        return SHULKER_BOXES.contains(item);
    }
}
