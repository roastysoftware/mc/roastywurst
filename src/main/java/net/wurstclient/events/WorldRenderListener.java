/*
 * Copyright (C) 2014 - 2020 | Alexander01998 | All rights reserved.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.events;

import net.minecraft.client.render.Camera;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.render.LightmapTextureManager;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.Matrix4f;
import net.wurstclient.event.Event;
import net.wurstclient.event.Listener;

import java.util.ArrayList;

public interface WorldRenderListener extends Listener {

	void onWorldRender(WorldRenderEvent event);
	
	public static class WorldRenderEvent extends Event<WorldRenderListener> {

		public MatrixStack matrixStack;
		public float tickDelta;
		public long limitTime;
		public boolean renderBlockOutline;
		public Camera camera;
		public GameRenderer gameRenderer;
		public LightmapTextureManager lightmapTextureManager;
		public Matrix4f matrix4f;
		public VertexConsumerProvider.Immediate immediate;

		public WorldRenderEvent(MatrixStack matrices, float tickDelta, long limitTime, boolean renderBlockOutline,
								Camera camera, GameRenderer gameRenderer, LightmapTextureManager lightmapTextureManager,
								Matrix4f matrix4f, VertexConsumerProvider.Immediate immediate) {
			this.matrixStack = matrices;
			this.tickDelta = tickDelta;
			this.limitTime = limitTime;
			this.renderBlockOutline = renderBlockOutline;
			this.camera = camera;
			this.gameRenderer = gameRenderer;
			this.lightmapTextureManager = lightmapTextureManager;
			this.matrix4f = matrix4f;
			this.immediate = immediate;
		}

		@Override
		public void fire(ArrayList<WorldRenderListener> listeners) {
			listeners.forEach(listener -> listener.onWorldRender(this));
		}
		
		@Override
		public Class<WorldRenderListener> getListenerType() {
			return WorldRenderListener.class;
		}
	}

}
