/*
 * Copyright (C) 2014 - 2020 | Alexander01998 | All rights reserved.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.events;

import net.minecraft.client.gui.screen.Screen;
import net.wurstclient.event.Event;
import net.wurstclient.event.Listener;

import java.util.ArrayList;

public interface OpenContainerScreenListener extends Listener
{
	public void onOpenContainerScreen(OpenScreenEvent event);
	
	public static class OpenScreenEvent extends Event<OpenContainerScreenListener> {

		public Screen screen;

		public OpenScreenEvent(Screen screen) {
			this.screen = screen;
		}

		@Override
		public void fire(ArrayList<OpenContainerScreenListener> listeners)
		{
			for(OpenContainerScreenListener listener : listeners)
			{
				listener.onOpenContainerScreen(this);
				
				if(isCancelled())
					break;
			}
		}
		
		@Override
		public Class<OpenContainerScreenListener> getListenerType()
		{
			return OpenContainerScreenListener.class;
		}
	}
}
