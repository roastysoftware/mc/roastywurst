/*
 * Copyright (C) 2014 - 2020 | Alexander01998 | All rights reserved.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.events;

import net.wurstclient.event.Event;
import net.wurstclient.event.Listener;

import java.util.ArrayList;

public interface CameraGetFieldListener extends Listener
{
	public void onGetPitch(CameraGetFieldEvent event);
	
	public static class CameraGetFieldEvent extends Event<CameraGetFieldListener>
	{

		private Float pitch;

		@Override
		public void fire(ArrayList<CameraGetFieldListener> listeners)
		{
			for (CameraGetFieldListener listener : listeners) {
				if (pitch != null) {
					listener.onGetPitch(this);
				}
			}
		}
		
		@Override
		public Class<CameraGetFieldListener> getListenerType()
		{
			return CameraGetFieldListener.class;
		}

		public void setPitch(float pitch) {
			this.pitch = pitch;
		}

		public float getPitch() {
			return pitch;
		}

	}
}
