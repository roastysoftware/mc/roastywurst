package net.wurstclient.events;

import net.minecraft.entity.MovementType;
import net.minecraft.util.math.Vec3d;
import net.wurstclient.event.Event;
import net.wurstclient.event.Listener;

import java.util.ArrayList;

public interface ClientMoveListener extends Listener {

    public void onClientMove(ClientMoveEvent event);

    public static class ClientMoveEvent extends Event<ClientMoveListener> {
        public MovementType type;
        public Vec3d vec3d;

        public ClientMoveEvent(MovementType type, Vec3d vec3d) {
            this.type = type;
            this.vec3d = vec3d;
        }

        @Override
        public void fire(ArrayList<ClientMoveListener> listeners) {
            listeners.forEach(l -> l.onClientMove(this));
        }

        @Override
        public Class<ClientMoveListener> getListenerType() {
            return ClientMoveListener.class;
        }
    }


}
