/*
 * Copyright (C) 2014 - 2020 | Alexander01998 | All rights reserved.
 *
 * This source code is subject to the terms of the GNU General Public
 * License, version 3. If a copy of the GPL was not distributed with this
 * file, You can obtain one at: https://www.gnu.org/licenses/gpl-3.0.txt
 */
package net.wurstclient.events;

import net.wurstclient.event.Event;
import net.wurstclient.event.Listener;

import java.util.ArrayList;

public interface PreWorldRenderListener extends Listener
{
	public void onPreWorldRender();
	
	public static class PreWorldRenderEvent extends Event<PreWorldRenderListener>
	{

		@Override
		public void fire(ArrayList<PreWorldRenderListener> listeners)
		{
			for(PreWorldRenderListener listener : listeners)
				listener.onPreWorldRender();
		}
		
		@Override
		public Class<PreWorldRenderListener> getListenerType()
		{
			return PreWorldRenderListener.class;
		}
	}
}
